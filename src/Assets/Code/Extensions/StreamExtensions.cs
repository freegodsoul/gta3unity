﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

public static class StreamExtensions
{
	// STREAM/READERS

	public static bool EOF( this Stream stream )
	{
		return stream.Position == stream.Length;
	}

	public static bool EOF( this BinaryReader reader )
	{
		return reader.BaseStream.EOF();
	}
}