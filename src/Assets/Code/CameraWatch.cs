﻿using UnityEngine;
using System.Collections;

public class CameraWatch : MonoBehaviour
{
	public const float MAX_TIME_DELTA = 1f / 10f;

	public Transform watchTarget;
	public float lerpSpeed = 8f;
	public Vector3 relativePosition = Vector3.up * 10f;
	public bool useStartRelativePosition = true;

	public bool useMinLimit = false;
	public float minLimitHeight = 5f;

	// Use this for initialization
	void Start()
	{
		if ( useStartRelativePosition && watchTarget )
		{
			relativePosition = transform.position - watchTarget.position;
		}
	}

	// Update is called once per frame
	void Update()
	{
		float dt = Mathf.Min( Time.deltaTime, MAX_TIME_DELTA );
		if ( watchTarget )
		{
			Vector3 targetPosition = watchTarget.position + relativePosition;
			if ( useMinLimit )
				targetPosition.y = Mathf.Max( targetPosition.y, minLimitHeight );

			Vector3 delta = targetPosition - transform.position;
			transform.localPosition += delta * lerpSpeed * dt;
		}
	}
}