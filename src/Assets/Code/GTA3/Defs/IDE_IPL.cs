﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace GTA3
{
	public enum ItemSection
	{
		// IPL
		SEC_INST,		// Расположение и свойства статических и динамических (object.dat) объектов карты.
		SEC_CULL,		// CULLING ZONES
		SEC_PICK,		// Эта секция создаёт пикапы оружия. Rockstar использовали эту секцию лишь для создания огнетушителей в ресторанах быстрого питания.

		// IDE
		SEC_OBJS,		// Обычные объекты карты.
		SEC_TOBJ,		// Временные Объекты Карты, используется для определения объектов карты, которые видны только в определенное время суток. Например свет от окон.
		SEC_PEDS,		// Пешеходы - модели людей, используемые в городском трафике, а так же в скриптах
		SEC_CARS,		// Секция CARS используется для размещения автомобиля на карте
		SEC_HIER,		// Used to define objects for use in cutscenes.
		SEC_2DFX,		// Used to add particle effects and simple ped behaviors to defined objects.
		SEC_PATH,		// Пути педов, автомобилей и лодок. Используется только в GTA VC. В GTA3 пути прописываются в IDE файлах
	}

	public enum ItemType
	{
		Object,
		TimeObject,
		Ped,
		Car,
		Wheel,
		Weapon,
		Path,
		CarComp,
		PedComp,
		Misc,
		Unknown
	}

	public enum ItemGroup
	{
		Peds,
		Cars,
		Wheels,
		Weapons,
		CarComps,
		PedComps,
		Misc,
		Unknown
	}

	[System.Serializable]
	public struct ItemPlacement
	{
		public ItemSection type;

		// INST
		public int id;
		public Vector3 position;
		public Vector3 scale;
		public Quaternion rotation;
	}

	[System.Serializable]
	public class ItemDef
	{
		public ItemType type;
		// OBJ,TOBJ,PEDS,CARS
		public int id;
		public string modelName;
		public string txdName;
	}

	[System.Serializable]
	public class ObjectDef : ItemDef
	{
		// OBJ, TOBJ
		public int objCount;
		public int flags;
		// TOBJ
		public float timeOn;
		public float timeOff;
		// Пути
		public List<PathDef> pathdefs;
	}

	// PEDS
	[System.Serializable]
	public class PedDef : ItemDef
	{
		public string defaultPedType;
		public string behavior;
		public string animGroup;
		public int carsCanDriveMask;
	}

	// CARS
	[System.Serializable]
	public class CarDef : ItemDef
	{
		public string carType;			// Type of vehicle, which includes car, boat, train, and plane. This data is related to hardcoded functions and must not be changed or else it might crash the game.
		public string handlingId;		// Name corresponding to its handling data in the handling.cfg file.
		public string gameName;			// Name corresponding to its GXT entry, case sensitive and must be seven characters or less! Invalid name will show up as MODEL missing
		public string carClass;			// Class of the vehicle.
		public int frequency;			// Frequency of the vehicle spawning randomly on the streets.
		public int lvl;					// unknown
		public int compRules;			// unknown
		public int wheelId;				// wheel id
		public float wheelScale;		// wheel scale
		public int planeLOD;			// ID number of LOD model – can be any valid object.
	}

	// PATH
	[System.Serializable]
	public class PathDef : ItemDef
	{
		public string pathType;			// "car", "ped"
		public PathDefNode[] nodes;		// path nodes
	}

	[System.Serializable]
	public struct PathDefNode
	{
		public PathDefNodeType type;
		public int linkTo;
		public int unknown1;
		public Vector3 position;	// local position relative to object
		public int unknown2;
		public int lanes1;
		public int lanes2;
	}

	public enum PathDefNodeType
	{
		None = 0,
		External = 1,
		Internal = 2
	}

	public static class IDE
	{
		public const int PATH_NODE_COUNT = 12;

		public static int[ , ] itemGroupIds = 
		{
			{ 0, 89 },		//Peds,
			{ 90, 150 },	//Vehicles,
			{ 160, 169 },	//Wheels,
			{ 170, 184 },	//Weapons,
			{ 190, 195 },	//CarComps,
			{ 196, 197 },	//PedComps,
			{ 198, 199 },	//Misc
		};
	}

	public class IDE_Data : ScriptableObject
	{
		public List<ObjectDef> objs = new List<ObjectDef>();
		public List<ObjectDef> tobjs = new List<ObjectDef>();
		public List<ObjectDef> wheels = new List<ObjectDef>();
		public List<ObjectDef> weapons = new List<ObjectDef>();
		public List<PedDef> peds = new List<PedDef>();
		public List<CarDef> cars = new List<CarDef>();
		//public List<PathDef> paths = new List<PathDef>();

		private Dictionary<int, ItemDef> _dict = null;
		public Dictionary<int, ItemDef> dict
		{
			get
			{
				if ( _dict == null )
				{
					_dict = new Dictionary<int,ItemDef>();
					foreach ( ObjectDef def in objs ) _dict[ def.id ] = def;
					foreach ( ObjectDef def in tobjs ) _dict[ def.id ] = def;
					foreach ( ObjectDef def in wheels ) _dict[ def.id ] = def;
					foreach ( ObjectDef def in weapons ) _dict[ def.id ] = def;
					foreach ( PedDef def in peds ) _dict[ def.id ] = def;
					foreach ( CarDef def in cars ) _dict[ def.id ] = def;
				}
				return _dict;
			}
		}

		public void Clear()
		{
			dict.Clear();
			objs.Clear();
			tobjs.Clear();
			wheels.Clear();
			weapons.Clear();
			peds.Clear();
			cars.Clear();
			//paths.Clear();
		}

		public void Add( ItemDef itemdef )
		{
			if ( itemdef.type != ItemType.Path && dict.ContainsKey( itemdef.id ) )
			{
				Log.WriteLine( "IDE_Data уже содержит ID: " + itemdef.id + " [new: " + itemdef.type + "]" );
				return;
			}

			if ( itemdef.type == ItemType.Unknown )
				return;

			if ( itemdef.type == ItemType.Path )
			{
				//paths.Add( ( PathDef ) itemdef );
				if ( dict.ContainsKey( itemdef.id ) )
				{
					ObjectDef obj = ( ObjectDef ) dict[ itemdef.id ];
					obj.pathdefs.Add( ( PathDef ) itemdef );
				}
				else
				{
					Debug.Log( "Pathdef не нашел объект: " + itemdef.id );
				}
				return;
			}
				
			dict[ itemdef.id ] = itemdef;
			switch ( itemdef.type )
			{
				case ItemType.Object: objs.Add( ( ObjectDef ) itemdef ); break;
				case ItemType.TimeObject: tobjs.Add( ( ObjectDef ) itemdef ); break;
				case ItemType.Wheel: objs.Add( ( ObjectDef ) itemdef ); break;
				case ItemType.Weapon: objs.Add( ( ObjectDef ) itemdef ); break;
				case ItemType.Ped: peds.Add( ( PedDef ) itemdef ); break;
				case ItemType.Car: cars.Add( ( CarDef ) itemdef ); break;
			}
		}
	}
}