﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Text;

namespace GTA3
{
	// Type can be one of the following object id:s
	public enum RwsSectionID
	{
		// Core
		ID_NA_OBJECT = 0x00,
		ID_STRUCT = 0x01,
		ID_STRING = 0x02,
		ID_EXTENSION = 0x03,
		ID_TEXTURE = 0x06,
		ID_MATERIAL = 0x07,
		ID_MATERIALLIST = 0x08,
		ID_ATOMICSECT = 0x09,
		ID_WORLD = 0x0B,
		ID_FRAMELIST = 0x0E,
		ID_GEOMETRY = 0x0F,
		ID_CLUMP = 0x10,
		ID_ATOMIC = 0x14,
		ID_TEXNATIVE = 0x15,
		ID_TEXDICT = 0x16,
		ID_GEOMETRYLIST = 0x1A,
		ID_HANIMANIMATION = 0x1B,
		ID_CHUNKGROUPSTART = 0x29,
		ID_CHUNKGROUPEND = 0x2A,

		// Criterion Toolkit
		ID_MORPHPLUGIN = 0x0105,
		ID_SKINPLUGIN = 0x0116,
		ID_HANIMPLUGIN = 0x011e,
		ID_USERDATAPLUGIN = 0x011f,

		// Criterion World
		ID_BINMESHPLUGIN = 0x050e,

		ID_FRAME = 0x0253F2FE
	}

	public enum RwsRasterFormat
	{
		RF_DEFAULT = 0x0000,
		RF_1555 = 0x0100,
		RF_565 = 0x0200,
		RF_4444 = 0x0300,
		RF_LUM8 = 0x0400,
		RF_8888 = 0x0500,
		RF_888 = 0x0600,
		RF_16 = 0x0700,
		RF_24 = 0x0800,
		RF_32 = 0x0900,
		RF_555 = 0x0a00,

		RF_FLAG_AUTOMIPMAP = 0x1000,
		RF_FLAG_PAL8 = 0x2000,
		RF_FLAG_PAL4 = 0x4000,
		RF_FLAG_MIPMAP = 0x8000,

		RF_MASK_FORMAT = 0x0FFF,
		RF_MASK_FLAGS = 0xF000
	}

	public enum RwsGeometryFlags
	{
		GF_TRIANGLE_STRIP = 1,			// Is triangle strip (if disabled it will be an triangle list)
		GF_XYZ = 2,						// Position
		GF_UV1 = 4,						// UV (в геометрии есть секция UV Mapping level 1)
		GF_DIFFUSE = 8,					// Vertex colors
		GF_NORMALS = 16,				// Store normals
		GF_DYNAMIC_LIGHTING = 32,		// определяет некий dynamic vertex lightning
		GF_MATERIAL_COLOR = 64,			// цвет материала
		GF_UV2 = 128,					// в геометрии есть секции UV Mapping level 1, UV Mapping level 2 и UV Mapping level 3
	}

	public class RwsSectionHeader
	{
		public int type;
		public int length;
		public int version;
		public int seekpos;
	}

	// geometry face (polygon)
	public struct RwsFace
	{
		public short v1;
		public short v2;
		public short v3;
		public short alpha;
	}

	// bounding sphere
	public struct RwsSphere
	{
		public Vector3 center;
		public float radius;
		public int hasPosition;
		public int hasNormals;
	}

	public struct RwsGeometryHeader
	{
		public short flags;
		public byte uvCount;
		public byte geomNativeFlags;
		public int faceCount;
		public int vertexCount;
		public int morphTargetCount;

		public Color32 ambientColor;
		public Color32 diffuseColor;
		public Color32 specularColor;
	}

	public class RwsFrame
	{
		public Vector3 rotationX;
		public Vector3 rotationY;
		public Vector3 rotationZ;
		public Vector3 position;
		public int parentFrame;
		public int matrixFlags;
		public string name;

		// LINKAGE
		public int linkedGeometry;
	}

	public class RwsGeometry
	{
		// HEADER
		public RwsGeometryHeader h;

		// DATA
		public Color32[] colors;
		public Vector2[] uv1;
		public Vector2[] uv2;
		public Vector2[] uv3;
		public RwsFace[] faces;
		public RwsSphere sphere;
		public Vector3[] vertices;
		public Vector3[] normals;
		public RwsMaterial[] materials;
	}

	public class RwsMaterial
	{
		// STRUCT
		public int unknown;			//4b - DWORD  - Unknown (usually 0)
		public Color32 color;		//4b - DWORD  - Color (as an D3DCOLOR value)
		public int unknown2;		//4b - DWORD  - Unknown (usually 1)
		public int textureCount;	//4b - DWORD  - Uses texture (TRUE/FALSE)
		public float ambient;		//4b - FLOAT  - Ambient
		public float specular;		//4b - FLOAT  - Specular
		public float diffuse;		//4b - FLOAT  - Diffuse

		// TEXTURES
		public RwsTexture[] textures;
		public int[] triangles;		// [3 * triangleCount] vertex indices
	}

	[Serializable]
	public struct RwsTexture
	{
		public short filterFlags;
		public short unknown;
		public string textureName;
		public string alphaName;
	}

	public struct RwsAtomic
	{
		public int frameIndex;
		public int geometryIndex;
		public int unknown1;
		public int unknown2;
	}

	public struct RwsTextureNativeHeader
	{
		public uint platformID;
		public ushort filterFlags;
		public byte wrapModeU;
		public byte wrapModeV;
		public string name;
		public string maskName;
		public uint rasterFormat;
		public uint hasAlpha;
		public ushort width;
		public ushort height;
		public byte bpp; // bits per pixel
		public byte mipmapLevels;
		public byte rasterType;
		public byte compression;
	}
}