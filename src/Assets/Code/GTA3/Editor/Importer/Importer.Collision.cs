﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace GTA3
{
	public partial class Importer /* .Collision */
	{
		public Dictionary<string, Transform> collisions = new Dictionary<string, Transform>();

		public Transform ImportCollisions( string colname )
		{
			string colpath = GetSrcDataPath( SrcDataType.Collision, colname );
			return ImportCollisionsAtPath( colpath );
		}

		public Transform ImportCollision( string colname, string modelname )
		{
			string colpath = GetSrcDataPath( SrcDataType.Collision, colname );
			return ImportCollisionAtPath( colpath, modelname );
		}

		public Transform ImportCollisionsAtPath( string colpath )
		{
			string colname = Path.GetFileNameWithoutExtension( colpath );
			COLReader r = new COLReader( colpath );
			r.Read();
			r.End();

			Transform root = new GameObject( colname ).transform;

			Progress_Start( "Importing Collisions: " + colpath );

			float x = 0f;
			for ( int i = 0; i < r.models.Length; i++ )
			{
				COL_Model cm = r.models[ i ];

				if ( Progress_Update( i + " / " + r.models.Length + ": " + cm.name, ( float ) i / ( float ) r.models.Length ) )
					break;

				Transform t = CreateCollisionModel( cm );
				t.parent = root;

				const float spacing = 0.5f;
				Bounds bounds = t.transform.HierarchyBounds();
				float dy = bounds.center.y - bounds.extents.y;

				x += bounds.extents.x + spacing;
				t.transform.Translate( x, -dy, 0f, Space.Self );
				x += bounds.extents.x + spacing;
			}

			Progress_Stop();

			return root;
		}

		public Transform ImportCollisionAtPath( string colpath, string modelname )
		{
			string colname = Path.GetFileNameWithoutExtension( colpath );
			COLReader r = new COLReader( colpath );
			r.Read();
			r.End();

			foreach ( COL_Model cm in r.models )
				if ( cm.name.ToLower() == modelname.ToLower() )
					return CreateCollisionModel( cm );

			return null;
		}

		public Transform CreateCollisionModel( COL_Model cm )
		{
			GameObject go = new GameObject( cm.name );

			if ( createMeshColliders )
			{
				Mesh mesh = null;

				if ( customMeshColliders )
				{
					Transform t = customMeshColliders.FindChild( cm.name );
					if ( t )
					{
						MeshFilter mf = t.GetComponent<MeshFilter>();
						if ( mf )
							mesh = mf.sharedMesh;
					}
				}
				else if ( cm.faces != null && cm.faces.Length > 0 )
				{
					mesh = new Mesh();
					mesh.name = cm.name;

					// vertices
					Vector3[] vertices = new Vector3[ cm.vertices.Length ];
					for ( int i = 0; i < vertices.Length; i++ )
						vertices[ i ] = MathUtils.LeftHand( cm.vertices[ i ].v );

					// triangles
					int[] triangles = new int[ cm.faces.Length * 3 ];
					for ( int i = 0; i < cm.faces.Length; i++ )
					{
						int tIndex = i * 3;
						triangles[ tIndex + 0 ] = cm.faces[ i ].v1;
						triangles[ tIndex + 1 ] = cm.faces[ i ].v2;
						triangles[ tIndex + 2 ] = cm.faces[ i ].v3;
					}

					mesh.vertices = vertices;
					mesh.triangles = triangles;

					mesh.RecalculateNormals();
					mesh.RecalculateBounds();
				}

				if ( mesh )
				{
					MeshCollider mc = go.AddComponent<MeshCollider>();
					mc.sharedMesh = mesh;
				}
			}

			if ( createSphereColliders )
			{
				foreach ( COL_Sphere s in cm.spheres )
				{
					SphereCollider sc = go.AddComponent<SphereCollider>();
					sc.radius = s.radius;
					sc.center = MathUtils.LeftHand( s.center );
				}
			}

			if ( createBoxColliders )
			{
				foreach ( COL_Box b in cm.boxes )
				{
					BoxCollider bc = go.AddComponent<BoxCollider>();
					bc.center = MathUtils.LeftHand( b.min + b.max ) * 0.5f;
					bc.size = MathUtils.LeftHand( b.max - b.min );
				}
			}

			collisions[ cm.name ] = go.transform;
			return go.transform;
		}
	}
}