﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace GTA3
{
	public partial class Importer /* .Texture */
	{
		public Dictionary<string, string> importedTxds = new Dictionary<string, string>();		// cached txd
		
		public Dictionary<string, Texture> _textures;		// cached textures
		public Dictionary<string, Texture> textures
		{
			get
			{
				if ( _textures == null )
				{
					_textures = new Dictionary<string, Texture>();
					string[] paths = AssetUtils.FindFilesByFilter( ASSET_ROOT + ASSET_MODELS, "*.png" );
					foreach ( string path in paths )
					{
						LoadTextureAtPath( path );
					}
				}

				return _textures;
			}
		}

		public Texture GetTexture( string texname )
		{
			if ( textures.ContainsKey( texname ) )
				return textures[ texname ];
			return null;
		}
			
		public Texture GetTexture( string txdname, string texname )
		{
			if ( textures.ContainsKey( texname ) )
				return textures[ texname ];
			return LoadTexture( txdname, texname );
		}

		public Texture LoadTexture( string txdname, string texname /*, bool importIfNonLoaded = true*/ )
		{
			Texture tex = null;
			bool isgeneric = IsTxdGeneric( txdname );
			if ( isgeneric )
			{
				foreach ( string genname in genericTxdNames )
				{
					string texpath = GetAssetPath( ItemType.Unknown, txdname, AssetType.Texture, genname );
					tex = LoadTextureAtPath( texpath );
					if ( tex )
						return tex;
				}
			}
			else
			{
				string texpath = GetAssetPath( ItemType.Unknown, txdname, AssetType.Texture, texname );
				tex = LoadTextureAtPath( texpath );
				if ( tex )
					return tex;
			}

			//if ( importIfNonLoaded && !importedTxds.ContainsKey( txdname ) )
			//{
			//	ImportTXD( txdname, true );
			//	if ( textures.ContainsKey( texname ) )
			//		return textures[ texname ];
			//}

			return null;
		}

		public Texture LoadTextureAtPath( string texpath )
		{
			Texture tex = AssetUtils.LoadTexture( texpath );
			if ( tex )
			{
				textures[ tex.name ] = tex;
				return tex;
			}

			return null;
		}

		public Texture[] ImportTXD( string txdname, ItemDef itemdef = null )
		{
			string path = GetSrcDataPath( SrcDataType.TexDict, txdname );
			Texture[] texs = ImportTxdAtPath( path, itemdef );
			importedTxds[ txdname ] = txdname;
			return texs;
		}

		//public void ImportTXD( string txdname )
		//{
		//	bool isgeneric = IsTxdGeneric( txdname );
		//	if ( isgeneric )
		//	{
		//		foreach ( string genname in genericTxdNames )
		//		{
		//			string path = GetTxdPath( txdname, isgeneric );
		//			ImportTxdAtPath( path );
		//		}
		//	}
		//	else
		//	{
		//		string path = GetTxdPath( txdname, isgeneric );
		//		ImportTxdAtPath( path );
		//	}
		//	importedTxds[ txdname ] = txdname;
		//}

		public Texture[] ImportTxdAtPath( string txdpath, ItemDef itemdef = null )
		{
			if ( !File.Exists( txdpath ) )
			{
				Debug.Log( "Не найден TXD\n" + txdpath );
				return null;
			}

			string txdname = Path.GetFileNameWithoutExtension( txdpath );

			TXDReader txdReader = new TXDReader( txdpath, textureFilter );
			txdReader.Read();
			txdReader.End();

			if ( txdReader.textures == null )
				return null;

			if ( createTextureAssets )
			{
				// SAVE & LOAD ASSETS

				string[] texnames = new string[ txdReader.textures.Length ];
				bool[] hasAlphas = new bool[ txdReader.textures.Length ];

				Progress_Start( "Creating Texture Assets: " + txdpath );
				for ( int i = 0; i < txdReader.textures.Length; i++ )
				{
					Texture2D tex = ( Texture2D ) txdReader.textures[ i ];

					if ( Progress_Update( i + " / " + txdReader.textures.Length + ": " + tex.name, ( float ) i / ( float ) txdReader.textures.Length ) )
						break;

					texnames[ i ] = tex.name;
					hasAlphas[ i ] = tex.alphaIsTransparency;

					string texpath = itemdef != null ?
						GetAssetPath( itemdef.type, txdname, AssetType.Texture, tex.name ) :
						GetAssetPath( ItemType.Unknown, txdname, AssetType.Texture, tex.name );

					AssetUtils.CreateAsset( tex, texpath );

					// LOAD ASSET

					// После вызова Refresh, текстура будет удалена (?)
					string texname = tex.name;
					AssetDatabase.Refresh();
					Texture2D texasset = ( Texture2D ) AssetUtils.LoadTexture( texpath );

					if ( hasAlphas[ i ] )
					{
						TextureImporter ti = ( TextureImporter ) AssetImporter.GetAtPath( texpath );
						ti.alphaIsTransparency = true;
					}

					texasset.alphaIsTransparency = hasAlphas[ i ];

					// CACHE
					textures[ texname ] = texasset;
				}
				Progress_Stop();

				// LOAD ASSETS (AND TWEAK)

				//AssetDatabase.Refresh();
				//for ( int i = 0; i < texnames.Length; i++ )
				//{
				//	string texpath = GetAssetPath( ItemType.Unknown, txdname, AssetType.Texture, texnames[ i ] );
				//	Texture2D texasset = ( Texture2D ) AssetUtils.LoadTexture( texpath );

				//	if ( hasAlphas[ i ] )
				//	{
				//		TextureImporter ti = ( TextureImporter ) AssetImporter.GetAtPath( texpath );
				//		ti.alphaIsTransparency = true;
				//	}

				//	texasset.alphaIsTransparency = hasAlphas[ i ];
				//	textures[ texnames[ i ] ] = texasset;
				//}
			}
			else
			{
				// CACHE
				foreach ( Texture tex in txdReader.textures )
				{
					textures[ tex.name ] = tex;
				}
			}

			return txdReader.textures;
		}

	}
}