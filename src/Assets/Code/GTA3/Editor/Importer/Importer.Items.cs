﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace GTA3
{
	public partial class Importer /* .Items */
	{
		// IDE
		private IDE_Data _itemdefs;
		public IDE_Data itemdefs
		{
			get
			{
				if ( _itemdefs == null )
				{
					_itemdefs = GetIDEInstance();
					_itemdefs.Clear();
					ImportItemDefs( PATH_DATA + "default.ide" ); // Peds, Cars
					ImportItemDefs( PATH_MAPS + "generic.ide" ); // Maps, Objs
				}
				return _itemdefs;
			}
		}

		/// <summary>
		/// Функция создает префаб, основываясь на:
		/// 1) Типе айтема, полученного из ide
		/// 2) Модели
		/// 3) Текстуре
		/// и возвращает ссылку на загруженный префаб
		/// </summary>
		/// <param name="id"></param>
		/// <param name="mapname"></param>
		/// <param name="q"></param>
		public Item ImportItem( int id, bool destroySceneGO, string colname, string idename )
		{
			// Получаем ItemDef из файла itemdefs.asset

			ItemDef itemdef = GetItemDef( id );
			if ( itemdef == null )
			{
				string idepath = GetSrcDataPath( SrcDataType.ItemDef, idename );
				ImportItemDefs( idepath );
				itemdef = GetItemDef( id );
				if ( itemdef == null )
				{
					Debug.LogError( "ItemDef не найден c таким ID: [" + id + "]" );
					return null;
				}
			}

			if ( itemdef.type == ItemType.Wheel || itemdef.type == ItemType.Weapon )
			{
				Debug.Log( "Для импорта колес и оружия используйте функцию Import Generics" );
				return null;
			}

			// Импортируем текстуры
			ImportTXD( itemdef.txdName, itemdef );

			// Collision Model
			if ( createBoxColliders || createSphereColliders || createMeshColliders )
			{
				if ( colname != "" )
					ImportCollision( colname, itemdef.modelName );
			}

			// Создаем модель в сцене, форсировано отключая создание префаба (т.к. позднее создадим для айтема (мб))
			string dffName = itemdef.modelName;
			bool oldCreateModelPrefabs = createModelPrefabs;
			createModelPrefabs = false;
			Frame sceneModel = ImportModel( dffName, true, true );
			createModelPrefabs = oldCreateModelPrefabs;

			// Вешаем компонент Item в зависимости от типа
			Item item;
			switch ( itemdef.type )
			{
				case ItemType.Car:
					item = Construct_Car( sceneModel, ( CarDef ) itemdef );
					break;

				case ItemType.Ped:
					Ped ped = sceneModel.gameObject.AddComponent<Ped>();
					item = ped;
					break;

				default:
					item = sceneModel.gameObject.AddComponent<Item>();
					break;
			}
			item.itemdef = itemdef;

			if ( createItemPrefabs )
			{
				// Создаем ассет префаба
				string prefabPath = GetAssetPath( itemdef.type, dffName, AssetType.Prefab, itemdef.modelName );
				AssetUtils.CreateAsset( sceneModel.gameObject, prefabPath );
				//Object prefab = PrefabUtility.GetPrefabObject( itemModel.gameObject );

				// Удаляем остатки
				if ( destroySceneGO )
				{
					GameObject.DestroyImmediate( sceneModel.gameObject );
					return null;
				}
			}

			return item;
		}

		private IPLParser ImportItemPlacements( string path )
		{
			IPLParser parser = new IPLParser();
			parser.Execute( path );
			return parser;
		}

		public Item CreateItem( int id )
		{
			return CreateItem( id, Vector3.zero, Quaternion.identity, Vector3.one, mapRoot );
		}

		public Item CreateItem( ItemPlacement pl )
		{
			return CreateItem( pl.id, MathUtils.LeftHand( pl.position ), MathUtils.LeftHand( pl.rotation ), MathUtils.LeftHand( pl.scale ), mapRoot );
		}

		public Item CreateItem( int id, Vector3 position, Quaternion rotation, Vector3 scale, Transform parent )
		{
			ItemDef itemdef = GetItemDef( id );
			if ( itemdef == null )
			{
				Debug.LogError( "ItemDef не найден c таким ID: [" + id + "]" );
				return null;
			}

			if ( itemdef.modelName.StartsWith( "LOD" ) || itemdef.modelName.EndsWith( "LOD" ) ||
				itemdef.modelName.StartsWith( "lod" ) || itemdef.modelName.EndsWith( "lod" ) )
			{
				return null;
			}

			Item item = ImportItem( id, false, "", "" );

			//Object modelPrefab = GetModelPrefab( itemdef.modelName );
			//if ( !modelPrefab )
			//{
			//	Debug.LogError("не удалось загрузить префаб: " + itemdef.modelName);
			//	return null;
			//}

			//GameObject go = (GameObject) GameObject.Instantiate( modelPrefab );

			item.transform.parent = parent;
			item.transform.localPosition = position;
			item.transform.localRotation = rotation;
			item.transform.localScale = scale;

			//foreach ( Transform child in item.transform )
			//{
			//	if ( child.name.EndsWith( "_L0" ) )
			//	{
			//		child.localPosition = Vector3.zero;
			//		child.localRotation = Quaternion.identity;
			//	}
			//}

			//// Пути

			//if ( itemdef.type == ItemType.Object ||
			//	itemdef.type == ItemType.TimeObject )
			//{
			//	ObjectDef obj = ( ObjectDef ) itemdef;
			//	foreach ( PathDef path in obj.pathdefs )
			//	{
			//		GameObject path_go = go.CreateChild( "path_" + path.pathType );
			//		ItemPath ip = path_go.AddComponent<ItemPath>();
			//		ip.type = path.pathType;
			//		ip.nodes = path.nodes;
			//	}
			//}

			//return item;

			return null;
		}

		//-------------------------------------------------------------------
		// ITEM DEFINITIONS (IDE)
		//-------------------------------------------------------------------

		private IDE_Data ImportItemDefs( string path )
		{
			IDEParser parser = new IDEParser();
			parser.Execute( path );
			return parser.ide;
		}

		//public IDE_Data GetItemDefData( string idename )
		//{
		//	if ( !itemdefs.ContainsKey( idename ) )
		//	{
		//		// Парсим напрямую из файла
		//		string path;
		//		switch ( idename.ToLower() )
		//		{
		//			case "default":
		//				path = PATH_DATA + idename + ".ide";
		//				break;

		//			case "generic":
		//			case "gta3":
		//				path = PATH_MAPS + idename + ".ide";
		//				break;

		//			default:
		//				path = PATH_MAPS + idename + "/" + idename + ".ide";
		//				break;
		//		}

		//		itemdefs[ idename ] = ImportItemDefs( path );
		//	}
		//	return itemdefs[ idename ];
		//}

		public ItemDef GetItemDef( int id )
		{
			//foreach ( IDE_Data ide in itemdefs.Values )
			//	if ( ide.dict.ContainsKey( id ) )
			//		return ide.dict[ id ];

			if ( itemdefs.dict.ContainsKey( id ) )
				return itemdefs.dict[ id ];

			return null;
		}

		public static IDE_Data GetIDEInstance()
		{
			string path = ASSET_ROOT + "itemdefs.asset";
			IDE_Data data = ( IDE_Data ) AssetDatabase.LoadAssetAtPath( path, typeof( IDE_Data ) );
			if ( data == null )
			{
				data = ScriptableObject.CreateInstance<IDE_Data>();
				AssetUtils.CreateAsset( data, path );
				AssetDatabase.Refresh();
			}

			return data;
		}

	}
}