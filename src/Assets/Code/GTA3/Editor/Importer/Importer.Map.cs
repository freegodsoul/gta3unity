﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace GTA3
{
	public partial class Importer /* .Map */
	{
		private List<Item> mapItems;
		private Transform mapRoot;

		public void ImportMapAtPath( string iplpath )
		{
			string ipldir = Path.GetDirectoryName( iplpath );
			string iplname = Path.GetFileNameWithoutExtension( iplpath );

			// Импортируем IPL
			IPLParser iplParser = new IPLParser();
			iplParser.Execute( iplpath );

			// Читаем ide-данные
			//string idepath = ipldir + "/" + iplname + ".ide";
			//if ( File.Exists( idepath ) )
			//	ImportItemDefs( idepath );
			foreach ( string idepath in idePaths )
				ImportItemDefs( idepath );

			// Читаем col-данные
			//string colpath = ipldir + "/" + iplname + ".col";
			//if ( File.Exists( colpath ) )
			//	ImportCollisionsAtPath( colpath );

			// Проверяем на наличие всех IDE, на которые ссылается IPL
			bool skipMapImport = false;
			foreach ( ItemPlacement ipl in iplParser.placements )
			{
				if ( GetItemDef( ipl.id ) == null )
				{
					Log.WriteLine( "ItemDef с таким id не загружен в память: " + ipl.id );
					skipMapImport = true;
				}
			}
			if ( skipMapImport )
				return;

			// Map Root
			mapRoot = new GameObject( iplname ).transform;

			// Создаем айтемы
			Progress_Start( "Items Placing: " + iplpath );
			List<ItemPlacement> placements = iplParser.placements;
			mapItems = new List<Item>();
			for ( int i = 0; i < placements.Count; i++ )
			{
				string progressStatus = i + " / " + placements.Count;
				
				ItemDef itemdef = GetItemDef( placements[ i ].id );
				if ( itemdef != null )
					progressStatus += ": " + itemdef.modelName;

				if ( Progress_Update( progressStatus, ( float ) i / ( float ) placements.Count ) )
					break;

				Item item = CreateItem( placements[ i ] );
				if ( item != null )
					mapItems.Add( item );
			}
			Progress_Stop();
			Debug.Log( "items created: " + mapItems.Count + "/" + placements.Count );
		}
	}
}