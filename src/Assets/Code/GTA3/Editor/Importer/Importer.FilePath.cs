﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

namespace GTA3
{
	//-------------------------------------------------------------------
	// FILE PATH WORKING
	//-------------------------------------------------------------------

	public enum SrcDataType
	{
		Model,		// *.dff
		TexDict,	// *.txd
		Collision,	// *.col
		Animation,	// *.ifp
		ItemDef,	// *.ide
		ItemPlace,	// *.ipl
		Config,		// *.cfg
		Data,		// *.dat
	}

	public partial class Importer /* .FilePath */
	{
		// Native GTA3 Data
		public const string PATH_ROOT = "../data/";
		public const string PATH_ANIM = PATH_ROOT + "anim/";
		public const string PATH_MAPS = PATH_ROOT + "maps/";
		public const string PATH_DATA = PATH_ROOT + "data/";
		public const string PATH_PATHS = PATH_ROOT + "paths/";
		public const string PATH_MODELS = PATH_ROOT + "models/";
		public const string PATH_TEXTURES = PATH_ROOT + "textures/";
		public const string PATH_COLLISIONS = PATH_ROOT + "collisions/";

		// Imported Data by Asset Type
		public const string ASSET_ROOT = "Assets/Imported/";
		public const string ASSET_ANIM = "Anim/";
		public const string ASSET_MODELS = "Models/";
		//public const string ASSET_TEXTURES = "Textures/";
		//public const string ASSET_ITEMS = "Items/";

		// Model/Item sub dirs
		public const string MODEL_MESHES = "Meshes/";
		public const string MODEL_TEXTURES = "Textures/";
		public const string MODEL_MATERIALS = "Materials/";

		// Model/Item Group sub dirs
		//public const string GROUP_OBJECTS = "Objects/";
		//public const string GROUP_TOBJECTS = "TimeObjects/";
		//public const string GROUP_CARS = "Cars/";
		//public const string GROUP_CAR_COMPS = "CarComps/";
		//public const string GROUP_PEDS = "Peds/";
		//public const string GROUP_PED_COMPS = "PedComps/";
		//public const string GROUP_WHEELS = "Wheels/";
		//public const string GROUP_WEAPONS = "Weapons/";
		//public const string GROUP_MISC = "Misc/";
		//public const string GROUP_UNKNOWN = "Unknown/";
		
		//public const bool makeGenericSubDir = true;
		//public const bool makeSubDirs = true;


		// Закомментированны те, которые BAD импортируются, да и по сути не нужны
		public static string[] genericDffNames = new string[]
		{
			//"air_vlo",
			//"arrow",
			//"loplyguy",
			//"peds",
			//"qsphere",
			//"sphere",
			"weapons",
			"wheels",
			//"zonecyla",
			//"zonecylb",
			//"zonesphr"
		};
		
		public static string[] genericTxdNames = new string[]
		{
			//"fonts",
			//"frontend",
			"generic",
			//"hud",
			//"menu",
			"misc",
			//"particle",
			//"ronts"
		};

		public string[] iplNames = { };
		public string[] ideNames = { };
		public string[] dffNames = { };
		public string[] txdNames = { };
		public string[] colNames = { };
		public string[] ifpNames = { };
		public string[] cfgNames = { };
		public string[] datNames = { };

		public string[] iplPaths = { };
		public string[] idePaths = { };
		public string[] dffPaths = { };
		public string[] txdPaths = { };
		public string[] colPaths = { };
		public string[] ifpPaths = { };
		public string[] cfgPaths = { };
		public string[] datPaths = { };

		public void UpdateSrcDataPaths()
		{
			UpdateSrcDataPaths( out iplNames, out iplPaths, "*.ipl", PATH_MAPS );
			UpdateSrcDataPaths( out ideNames, out idePaths, "*.ide", PATH_MAPS, PATH_DATA );
			UpdateSrcDataPaths( out dffNames, out dffPaths, "*.dff", PATH_MODELS );
			UpdateSrcDataPaths( out txdNames, out txdPaths, "*.txd", PATH_TEXTURES );
			UpdateSrcDataPaths( out colNames, out colPaths, "*.col", PATH_COLLISIONS );
			UpdateSrcDataPaths( out ifpNames, out ifpPaths, "*.ifp", PATH_ANIM );
			UpdateSrcDataPaths( out cfgNames, out cfgPaths, "*.cfg", PATH_DATA );
			UpdateSrcDataPaths( out datNames, out datPaths, "*.dat", PATH_DATA );
		}

		public void UpdateSrcDataPaths( out string[] names, out string[] paths, string filter, params string[] dirs )
		{
			List<string> pathList = new List<string>();
			foreach ( string dir in dirs )
			{
				string[] append = AssetUtils.FindFilesByFilter( dir, filter );
				pathList.AddRange( append );
			}

			paths = pathList.ToArray();
			names = new string[ paths.Length ];
			
			for ( int i = 0; i < paths.Length; i++ )
				names[ i ] = Path.GetFileNameWithoutExtension( paths[ i ] );
		}

		public string GetSrcDataPath( SrcDataType type, string name )
		{
			int index = -1;
			switch ( type )
			{
				case SrcDataType.Model: index = dffNames.IndexOf( name, true ); break;
				case SrcDataType.TexDict: index = txdNames.IndexOf( name, true ); break;
				case SrcDataType.Collision: index = colNames.IndexOf( name, true ); break;
				case SrcDataType.Animation: index = ifpNames.IndexOf( name, true ); break;
				case SrcDataType.ItemDef: index = ideNames.IndexOf( name, true ); break;
				case SrcDataType.ItemPlace: index = iplNames.IndexOf( name, true ); break;
				case SrcDataType.Config: index = cfgNames.IndexOf( name, true ); break;
				case SrcDataType.Data: index = datNames.IndexOf( name, true ); break;
			}

			return GetSrcDataPath( type, index );
		}

		public string GetSrcDataPath( SrcDataType type, int index )
		{
			switch ( type )
			{
				case SrcDataType.Model: return dffPaths[ index ];
				case SrcDataType.TexDict: return txdPaths[ index ];
				case SrcDataType.Collision: return colPaths[ index ];
				case SrcDataType.Animation: return ifpPaths[ index ];
				case SrcDataType.ItemDef: return idePaths[ index ];
				case SrcDataType.ItemPlace: return iplPaths[ index ];
				case SrcDataType.Config: return cfgPaths[ index ];
				case SrcDataType.Data: return datPaths[ index ];
			}

			throw new UnityException( "GetSrcDataPath(): unknown data type" );
		}

		/// <summary>
		/// Возвращает конечный путь импортируемого ассета
		/// </summary>
		/// <param name="itemType">не используется пока</param>
		/// <param name="dirName">имя файла (DFF/TXD) без расширения и директории</param>
		/// <param name="assetType"></param>
		/// <param name="assetName"></param>
		/// <returns></returns>
		public static string GetAssetPath( ItemType itemType, string dirName, AssetType assetType, string assetName )
		{
			string path = ASSET_ROOT;
			
			switch ( assetType )
			{
				case AssetType.Anim:
					path += ASSET_ANIM;
					break;

				case AssetType.Mesh:
				case AssetType.Prefab:
				case AssetType.Texture:
				case AssetType.Material:
					path += ASSET_MODELS;
					if ( IsDffGeneric( dirName ) || IsTxdGeneric( dirName ) )
						dirName = "generic";
					break;
			}
			

			if ( assetType != AssetType.Anim )
			{
				path += dirName + "/";

				if ( assetType != AssetType.Prefab )
				{
					switch ( assetType )
					{
						case AssetType.Mesh: path += MODEL_MESHES; break;
						case AssetType.Texture: path += MODEL_TEXTURES; break;
						case AssetType.Material: path += MODEL_MATERIALS; break;
					}
				}
			}

			path += assetName;

			switch ( assetType )
			{
				case AssetType.Anim: path += ".anim"; break;
				case AssetType.Mesh: path += ".asset"; break;
				case AssetType.Prefab: path += ".prefab"; break;
				case AssetType.Texture: path += ".png"; break;
				case AssetType.Material: path += ".mat"; break;
			}

			return path;
		}

		public static bool IsDffGeneric( string dffname )
		{
			dffname = dffname.ToLower();
			if ( dffname == "generic" )
				return true;

			foreach ( string genname in genericDffNames )
				if ( genname == dffname )
					return true;

			return false;
		}

		public static bool IsTxdGeneric( string txdname )
		{
			txdname = txdname.ToLower();
			if ( txdname == "generic" )
				return true;

			foreach ( string genname in genericTxdNames )
				if ( genname == txdname )
					return true;

			return false;
		}

		//public static string GetAssetPath( ItemType itemType, string itemName, AssetType assetType, string assetName )
		//{
		//	string path = ASSET_ROOT;

		//	// Если без типа айтема, тогда помещаем в Models/Textures/Anim
		//	if ( itemType == ItemType.Unknown )
		//	{
		//		switch ( assetType )
		//		{
		//			case AssetType.Anim: path += ASSET_ANIM; break;
		//			case AssetType.Texture: path += ASSET_TEXTURES; break;
		//			case AssetType.Mesh: path += ASSET_MODELS; break;
		//			case AssetType.Mesh: path += ASSET_MODELS; break;
		//		}
		//	}

		//	if ( itemType != ItemType.Unknown && itemName != "" )
		//	{
		//		path += ASSET_ITEMS;

		//		switch ( itemType )
		//		{
		//			case ItemType.Car: path += GROUP_CARS; break;
		//			case ItemType.CarComp: path += GROUP_CAR_COMPS; break;
		//			case ItemType.Object: path += GROUP_OBJECTS; break;
		//			case ItemType.TimeObject: path += GROUP_TOBJECTS; break;
		//			case ItemType.Ped: path += GROUP_PEDS; break;
		//			case ItemType.PedComp: path += GROUP_PED_COMPS; break;
		//			case ItemType.Weapon: path += GROUP_WEAPONS; break;
		//			case ItemType.Wheel: path += GROUP_WHEELS; break;
		//			case ItemType.Misc: path += GROUP_MISC; break;
		//		}

		//		path += itemName + "/";

		//		if ( assetType != AssetType.Prefab )
		//		{
		//			switch ( assetType )
		//			{
		//				case AssetType.Anim: path += ASSET_ANIM; break;
		//				case AssetType.Material: path += ASSET_MATERIALS; break;
		//				case AssetType.Mesh: path += ASSET_MESHES; break;
		//				case AssetType.Texture: path += ASSET_TEXTURES; break;
		//			}
		//		}
		//	}
		//	else
		//	{
		//		switch ( assetType )
		//		{
		//			case AssetType.Anim: path += ASSET_ANIM; break;
		//			case AssetType.Material: path += ASSET_MATERIALS; break;
		//			case AssetType.Mesh: path += ASSET_MESHES; break;
		//			case AssetType.Texture: path += ASSET_TEXTURES; break;
		//			case AssetType.Prefab: path += ASSET_PREFABS; break;
		//		}

		//		if ( itemName != "" )
		//		{
		//			path += itemName + "/";
		//		}
		//	}

		//	path += assetName;

		//	switch ( assetType )
		//	{
		//		case AssetType.Anim: path += ".anim"; break;
		//		case AssetType.Material: path += ".mat"; break;
		//		case AssetType.Mesh: path += ".asset"; break;
		//		case AssetType.Prefab: path += ".prefab"; break;
		//		case AssetType.Texture: path += ".png"; break;
		//	}

		//	return path;
		//}

		// NON USED ANYMORE
#if false

		public static string GetTxdPath( string txdname )
		{
			if ( IsTxdGeneric( txdname ) )
				return PATH_TEXTURES + "generic/" + txdname + ".txd";
			return PATH_TEXTURES + txdname + ".txd";
		}

		public static string GetDffPath( string dffname )
		{
			if ( IsDffGeneric( dffname ) )
				return PATH_MODELS + "generic/" + dffname + ".dff";
			return PATH_MODELS + dffname + ".dff";
		}

		public static string GetIfpPath( string ifpname )
		{
			return PATH_ANIM + ifpname + ".ifp";
		}

		public static string GetColPath( string colname )
		{
			return PATH_COLLISIONS + colname + ".col";
		}

		public static string GetTextureDirPath( string txdname )
		{
			if ( makeGenericSubDir && IsTxdGeneric( txdname ) )
			{
				if ( makeSubDirs )
					return ASSET_TEXTURES + "generic/" + txdname + "/";
				else
					return ASSET_TEXTURES + "generic/";

			}
			else
			{
				if ( makeSubDirs )
					return ASSET_TEXTURES + txdname + "/";
				else
					return ASSET_TEXTURES;
			}
		}

		public static string GetTexturePath( string txdname, string texname )
		{
			return GetTextureDirPath( txdname ) + texname + ".png";
		}

		public static string GetPrefabPath( string modelname )
		{
			if ( makeGenericSubDir && IsDffGeneric( modelname ) )
			{
				if ( makeSubDirs )
					return ASSET_PREFABS + "generic/" + modelname + ".prefab";
				else
					return ASSET_PREFABS + "generic/" + modelname + ".prefab";
			}
			else
			{
				if ( makeSubDirs )
					return ASSET_PREFABS + modelname + ".prefab";
				else
					return ASSET_PREFABS + modelname + ".prefab";
			}
		}

		public static string GetMeshPath( string modelname, string meshname )
		{
			if ( makeGenericSubDir && IsDffGeneric( modelname ) )
			{
				if ( makeSubDirs )
					return ASSET_MESHES + "generic/" + modelname + "/" + meshname + ".asset";
				else
					return ASSET_MESHES + "generic/" + meshname + ".asset";
			}
			else
			{
				if ( makeSubDirs )
					return ASSET_MESHES + modelname + "/" + meshname + ".asset";
				else
					return ASSET_MESHES + meshname + ".asset";
			}
		}

		public static string GetMaterialPath( string modelname, string matname )
		{
			if ( makeGenericSubDir && IsDffGeneric( modelname ) )
			{
				if ( makeSubDirs )
					return ASSET_MATERIALS + "generic/" + modelname + "/" + matname + ".mat";
				else
					return ASSET_MATERIALS + "generic/" + matname + ".mat";
			}
			else
			{
				if ( makeSubDirs )
					return ASSET_MATERIALS + modelname + "/" + matname + ".mat";
				else
					return ASSET_MATERIALS + matname + ".mat";
			}
		}
#endif
	}
}