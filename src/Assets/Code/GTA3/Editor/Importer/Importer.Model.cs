﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace GTA3
{
	//-------------------------------------------------------------------
	// MODEL IMPORTER (DFF)
	//-------------------------------------------------------------------

	public partial class Importer /* .Model */
	{
		public static Color32 COLOR_CAR_BODY_GREEN = new Color32( 60, 255, 0, 255 );
		public static Color32 COLOR_CAR_BODY_PURPLE = new Color32( 255, 0, 175, 255 );

		public static Color32 COLOR_CAR_BODY_GREEN_REPLACE = new Color32( 134, 153, 144, 255 );
		public static Color32 COLOR_CAR_BODY_PURPLE_REPLACE = new Color32( 107, 107, 107, 255 );

		private Dictionary<string, Object> _modelPrefabs;			// cached models
		public Dictionary<string, Object> modelPrefabs
		{
			get
			{
				if ( _modelPrefabs == null )
				{
					_modelPrefabs = new Dictionary<string, Object>();
					string[] paths = AssetUtils.FindFilesByFilter( ASSET_ROOT + ASSET_MODELS, "*.prefab" );
					foreach ( string path in paths )
					{
						LoadModelPrefabAtPath( path );
					}
				}

				return _modelPrefabs;
			}
		}

		// Current DFF data
		private Frame[] frames;
		private Mesh[] meshes;
		
		private List<Material> unitedMaterials;
		private List<string> unitedTextureNames;

		public Object GetModelPrefab( string modelName )
		{
			if ( modelPrefabs.ContainsKey( modelName ) )
				return modelPrefabs[ modelName ];
			return null;
		}

		public Object GetModelPrefab( string dffName, string modelName )
		{
			if ( modelPrefabs.ContainsKey( modelName ) )
				return modelPrefabs[ modelName ];
			return LoadModelPrefab( dffName, modelName );
		}

		public Object LoadModelPrefab( string dffName, string modelName )
		{
			string prefabPath = GetAssetPath( ItemType.Unknown, dffName, AssetType.Prefab, modelName );
			return LoadModelPrefabAtPath( prefabPath );
		}

		public Object LoadModelPrefabAtPath( string prefabPath )
		{
			Object prefab = AssetDatabase.LoadAssetAtPath( prefabPath, typeof( Object ) );
			if ( prefab )
			{
				modelPrefabs[ prefab.name ] = prefab;
				return prefab;
			}

			return null;
		}

		public void ImportGenerics( bool clearScene )
		{
			// Форсируем создание ассетов, если оно отключено

			bool oldСreateModelPrefabs = createModelPrefabs;
			bool oldCreateMeshAssets = createMeshAssets;
			bool oldCreateTextureAssets = createTextureAssets;
			bool oldCreateMaterialAssets = createMaterialAssets;

			createModelPrefabs = true;
			createMeshAssets = true;
			createTextureAssets = true;
			createMaterialAssets = true;

			// Импортируем текстуры в списке genericTxdNames, кэшируя

			foreach ( string txdname in genericTxdNames )
			{
				ImportTXD( txdname );
			}

			// Импортируем модели в списке genericDffNames,
			// создавая префабы моделей с текстурами, кэшируя

			foreach ( string dffname in genericDffNames )
			{
				Frame model = ImportModel( dffname, true, clearScene );
			}

			// Возвращаем опции

			createModelPrefabs = oldСreateModelPrefabs;
			createMeshAssets = oldCreateMeshAssets;
			createTextureAssets = oldCreateTextureAssets;
			createMaterialAssets = oldCreateMaterialAssets;

			// Выводим списки кэша

			Log.WriteLine( "\nGeneric Textures:" );
			foreach ( string name in textures.Keys )
				Log.WriteLine( name );

			Log.WriteLine( "\nGeneric Models:" );
			foreach ( string name in modelPrefabs.Keys )
				Log.WriteLine( name );
		}

		// Импортирует модель, опционально создает префаб и возвращает корневой фрейм (если не был установлен флаг destroySceneGO)
		public Frame ImportModel( string dffname, bool applyTextures, bool destroySceneGO )
		{
			// Читаем DFF
			Log.WriteLine( "Читаем DFF" );

			string dffpath = GetSrcDataPath( SrcDataType.Model, dffname );
			DFFReader reader = new DFFReader( dffpath );
			reader.Read();
			reader.End();

			// Создаем иерархию фреймов
			Log.WriteLine( "Создаем иерархию фреймов" );

			frames = new Frame[ reader.frameList.Length ];
			meshes = new Mesh[ reader.geometryList.Length ];
			
			unitedMaterials = new List<Material>();
			unitedTextureNames = new List<string>();

			for ( int i = 0; i < frames.Length; i++ )
			{
				CreateFrame( reader, i );
			}

			if ( applyTextures )
			{
				ApplyTexturesToModel( frames[ 0 ] );
			}

			if ( createModelPrefabs )
			{
				GameObject sceneGO = frames[ 0 ].gameObject;

				Progress_Start( "Importing: " + dffpath );
				if ( sceneGO.name == dffname )
				{
					CreateModelPrefab( sceneGO, dffname, sceneGO.name );
				}
				else
				{
					for ( int i = 0; i < sceneGO.transform.childCount; i++ )
					{
						Transform child = sceneGO.transform.GetChild( i );

						if ( Progress_Update( i + " / " + sceneGO.transform.childCount + ": " + child.name, ( float ) i / ( float ) sceneGO.transform.childCount ) )
							break;

						CreateModelPrefab( child.gameObject, dffname, child.name );
					}
				}
				Progress_Stop();

				if ( destroySceneGO )
				{
					GameObject.DestroyImmediate( sceneGO );
					return null;
				}
			}

			frames[ 0 ].name = dffname;
			return frames[ 0 ];
		}

		public Object CreateModelPrefab( GameObject go, string dffname, string modelName )
		{
			// CREATE PREFAB

			string prefabPath = GetAssetPath( ItemType.Unknown, dffname, AssetType.Prefab, modelName );
			AssetUtils.CreateAsset( go, prefabPath );

			// LOAD PREFAB

			AssetDatabase.Refresh();
			Object prefab = AssetDatabase.LoadAssetAtPath( prefabPath, typeof( Object ) );
			modelPrefabs[ modelName ] = prefab;

			return prefab;
		}

		public Frame CreateFrame( DFFReader reader, int frameIndex )
		{
			RwsFrame frame = reader.frameList[ frameIndex ];

			// Создаем GO
			GameObject go = new GameObject( frame.name );
			if ( frame.parentFrame != -1 )
			{
				Vector3 pos = MathUtils.LeftHand( frame.position );
				Quaternion rot = MathUtils.LeftHandQuat( frame.rotationX, frame.rotationY, frame.rotationZ );

				go.transform.parent = frame.parentFrame != -1 ? frames[ frame.parentFrame ].transform : null;
				go.transform.localPosition = pos;
				go.transform.localRotation = rot;
			}

			// Цепляем FRAME
			Frame ret = go.AddComponent<Frame>();
			frames[ frameIndex ] = ret;
			
			// Без геометрии наш фрейм пустой
			if ( frame.linkedGeometry == -1 )
				return ret;

			// Сломанные детали и LOD-модели прячем
			if ( frame.name.ToLower().EndsWith( "_dam" ) ||
				frame.name.ToLower().EndsWith( "_dama" ) ||
				frame.name.ToLower().EndsWith( "_l1" ) ||
				frame.name.ToLower().EndsWith( "_lo" ) ||
				frame.name.ToLower().EndsWith( "_vlo" ) )
			{
				go.SetActive( false );
			}

			// Создаем меш и материалы

			//if ( frame.linkedGeometry < 0 || frame.linkedGeometry >= reader.geometryList.Length )
			//	return null;

			Mesh mesh = CreateMesh( reader, frame.linkedGeometry, frame.name );
			go.SetMeshToGameObject( mesh, null );

			Material[] materials = CreateMaterials( reader, frameIndex, frame.linkedGeometry );
			go.SetSharedMaterialsToGameObject( materials );

			return ret;
		}

		public Mesh CreateMesh( DFFReader reader, int geomIndex, string name )
		{
			if ( meshes[ geomIndex ] != null )
				return meshes[ geomIndex ];

			RwsGeometry geom = reader.geometryList[ geomIndex ];

			//-------------------------------------------------------------
			// Создаем меш
			//-------------------------------------------------------------

			Mesh mesh = new Mesh();
			mesh.name = name;

			//---------------------------------------------------------
			// Vertices
			//---------------------------------------------------------

			Vector3[] vertices = new Vector3[ geom.vertices.Length ];
			for ( int i = 0; i < vertices.Length; i++ )
				vertices[ i ] = MathUtils.LeftHand( geom.vertices[ i ] );
			mesh.vertices = vertices;

			//---------------------------------------------------------
			// Color
			//---------------------------------------------------------

			mesh.colors32 = geom.colors;

			//---------------------------------------------------------
			// TRIANGLES (FACES)
			//---------------------------------------------------------

			bool triangleStrip = ( ( RwsGeometryFlags ) geom.h.flags & RwsGeometryFlags.GF_TRIANGLE_STRIP ) == RwsGeometryFlags.GF_TRIANGLE_STRIP;

			// SUBMESHES USED

			mesh.subMeshCount = geom.materials.Length;
			for ( int i = 0; i < geom.materials.Length; i++ )
			{
				int[] geomTriangles = geom.materials[ i ].triangles;
				int[] triangles;

				if ( triangleStrip )
				{
					List<int> triangleList = new List<int>();
					int triangleCount = geomTriangles.Length - 2;

					bool flipWinding = false;
					for ( int j = 0; j < triangleCount; j++ )
					{
						if ( geomTriangles[ j + 1 ] == geomTriangles[ j + 2 ] )
						{
							j++;
							flipWinding = false;
							continue;
						}

						//if ( ( j & 1 ) == 1 )
						if ( flipWinding )
						{
							triangleList.Add( geomTriangles[ j + 0 ] );
							triangleList.Add( geomTriangles[ j + 1 ] );
							triangleList.Add( geomTriangles[ j + 2 ] );
						}
						else
						{
							triangleList.Add( geomTriangles[ j + 0 ] );
							triangleList.Add( geomTriangles[ j + 2 ] );
							triangleList.Add( geomTriangles[ j + 1 ] );
						}

						flipWinding = !flipWinding;
					}
					
					triangles = triangleList.ToArray();
				}
				else
				{
					int triangleCount = geomTriangles.Length / 3;
					triangles = new int[ 3 * triangleCount ];

					for ( int j = 0; j < triangleCount; j++ )
					{
						int offset = 3 * j;
						triangles[ offset + 0 ] = geomTriangles[ offset + 0 ];
						triangles[ offset + 1 ] = geomTriangles[ offset + 2 ];
						triangles[ offset + 2 ] = geomTriangles[ offset + 1 ];
					}
				}
				mesh.SetTriangles( triangles, i );
			}

			//---------------------------------------------------------
			// UV
			//---------------------------------------------------------

			if ( geom.uv1 != null )
			{
				Vector2[] uv = new Vector2[ geom.uv1.Length ];
				for ( int i = 0; i < geom.uv1.Length; i++ )
				{
					uv[ i ].x = geom.uv1[ i ].x;
					uv[ i ].y = 1f - geom.uv1[ i ].y;
				}

				mesh.uv = uv;
			}

			//---------------------------------------------------------

			mesh.RecalculateNormals();
			mesh.RecalculateTangents();
			mesh.RecalculateBounds();

			meshes[ geomIndex ] = mesh;

			//---------------------------------------------------------
			// CREATE ASSET
			//---------------------------------------------------------

			if ( createMeshAssets )
			{
				string meshpath = GetAssetPath( ItemType.Unknown, reader.filename, AssetType.Mesh, mesh.name );
				AssetUtils.CreateAsset( mesh, meshpath );
			}

			return mesh;
		}

		public Material[] CreateMaterials( DFFReader reader, int frameIndex, int geomIndex )
		{
			RwsGeometry geom = reader.geometryList[ geomIndex ];
			Frame frame = frames[ frameIndex ];
			
			Material[] materials = new Material[ geom.materials.Length ];
			frame.textureNames = new RwsTexture[ materials.Length ];

			for ( int i = 0; i < materials.Length; i++ )
			{
				materials[ i ] = CreateMaterial( reader, frameIndex, geom, i );
			}

			return materials;
		}

		public Material CreateMaterial( DFFReader reader, int frameIndex, RwsGeometry geom, int matIndex )
		{
			RwsFrame rwsFrame = reader.frameList[ frameIndex ];
			RwsMaterial rwsMat = geom.materials[ matIndex ];
			Frame frame = frames[ frameIndex ];

			if ( rwsMat.color.Equals( COLOR_CAR_BODY_GREEN ) )
				rwsMat.color = COLOR_CAR_BODY_GREEN_REPLACE;
			else if ( rwsMat.color.Equals( COLOR_CAR_BODY_PURPLE ) )
				rwsMat.color = COLOR_CAR_BODY_PURPLE_REPLACE;

			string shaderName = IsTransparent( rwsMat ) ? "Transparent/Specular" : "Specular";
			Shader shader = Shader.Find( shaderName );
			Material mat = new Material( shader );
			mat.name = "";
			mat.color = ( Color ) rwsMat.color * rwsMat.diffuse;
			mat.SetSpecular( ( Color ) rwsMat.color * rwsMat.specular );

			// Загружаем текстуру
			if ( rwsMat.textureCount > 0 )
			{
				if ( rwsMat.textureCount > 1 )
					Debug.LogError( "Чето придумать надо, текстура не одна" );

				RwsTexture rwsTex = rwsMat.textures[ 0 ];
				mat.name = rwsTex.textureName;
				frame.textureNames[ matIndex ] = rwsTex;
			}
			
			//---------------------------------------------------------
			// Объединяем материал с уже существующим
			// - Захардкодить смысловые цвета материалов таких объектов, как корпусы машин (зеленый), стекла, металик (розовый)
			//---------------------------------------------------------

			//foreach ( Material umat in unitedMaterials )
			//{
			//	Debug.Log( mat );
			//	Debug.Log( umat );

			//	if ( mat.color != umat.color )
			//		continue;

			//	//if ( mat.GetSpecular() != umat.GetSpecular() )
			//	//	continue;

			//	if ( mat.shader.name != umat.shader.name )
			//		continue;

			//	if ( mat.name != "" && mat.name != umat.name )
			//		continue;

			//	return umat;
			//}

			//unitedMaterials.Add( mat );



			//---------------------------------------------------------
			// Создаем ассет материала объекта
			//---------------------------------------------------------

			if ( createMaterialAssets )
			{
				string matname = /*mat.name != "" ? mat.name : */ frame.name + "_mat_" + matIndex;
				string matpath = GetAssetPath( ItemType.Unknown, reader.filename, AssetType.Material, matname );
				AssetUtils.CreateAsset( mat, matpath );
			}

			return mat;
		}

		public void ApplyTexturesToModel( Frame frame )
		{
			// Применяем к текущему объекту
			MeshRenderer mr = frame.GetComponent<MeshRenderer>();
			if ( mr != null )
			{
				//Debug.Log( "SetTexturesToModel() " + frame.name+ "
				for ( int i = 0; i < frame.textureNames.Length; i++ )
				{
					string texname = frame.textureNames[ i ].textureName;
					if ( texname == null || texname == "" )
						continue;

					Material mat = mr.sharedMaterials[ i ];
					Texture2D tex = (Texture2D) GetTexture( texname );
					if ( !tex )
					{
						Debug.LogError( frame.name + " Не удалось получить текстуру [texname: " + texname + "]" );
						continue;
					}

					if ( tex.alphaIsTransparency )
						mat.shader = Shader.Find( "Transparent/Diffuse" );
					
					mat.mainTexture = tex;
					mr.sharedMaterials[ i ] = mat;
				}
			}
			
			// Применяем к детям
			for (int i = 0; i < frame.transform.childCount; i++ )
			{
				Frame child = frame.transform.GetChild( i ).GetComponent<Frame>();
				if ( child )
					ApplyTexturesToModel( child );
			}
		}

		public static bool IsTransparent( RwsMaterial mat )
		{
			if ( mat.color.a < 255 )
				return true;

			if ( mat.textureCount > 0 && mat.textures[ 0 ].alphaName != "" )
				return true;

			return false;
		}

	}
}