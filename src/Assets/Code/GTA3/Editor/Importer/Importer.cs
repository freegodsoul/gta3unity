﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace GTA3
{
	public partial class Importer
	{
		// preferences
		public float scale = 1f;
		public FilterMode textureFilter = FilterMode.Bilinear;

		public bool createModelPrefabs = false;
		public bool createItemPrefabs = false;
		
		public bool createMeshAssets = false;
		public bool createTextureAssets = false;
		public bool createMaterialAssets = false;

		public bool createSphereColliders = true;
		public bool createBoxColliders = true;
		public bool createMeshColliders = true;

		public Transform customMeshColliders;

		public Importer()
		{
		}

		public void Clear()
		{
			Log.Close();
			AssetDatabase.Refresh();

			if ( _modelPrefabs != null )
			{
				_modelPrefabs.Clear();
				_modelPrefabs = null;
			}

			if ( _textures != null )
			{
				_textures.Clear();
				_textures = null;
			}

			importedTxds.Clear();
			collisions.Clear();
		}

		//private void ImportFile( string path )
		//{
		//	// Предотвращаем зависание прогресс бара в случае исключения
		//	try
		//	{
		//		string ext = Path.GetExtension( path ).ToLower();

		//		if ( ext == ".dff" )
		//		{
		//			ImportModel( path );
		//		}
		//		else if ( ext == ".txd" )
		//		{
		//			ImportTXD( path );
		//		}
		//		else if ( ext == ".ipl" )
		//		{
		//			ImportItemPlacements( path );
		//		}
		//		else if ( ext == ".ide" )
		//		{
		//			ImportItemDefs( path );
		//		}
		//		else
		//		{
		//			Debug.LogError( "Unknown file extension\n" + path + " {" + ext + "}" );
		//		}
		//	}
		//	catch ( System.Exception ex )
		//	{
		//		Progress_Stop();
		//		throw ex;
		//	}

		//	LogUtils.Close();
		//}

		//-------------------------------------------------------------------
		// EDITOR MENU
		//-------------------------------------------------------------------

		//public static bool CheckAssetFileExtension( Object asset, string extension )
		//{
		//	if ( asset == null )
		//		return false;

		//	string path = AssetDatabase.GetAssetPath( Selection.activeObject );
		//	if ( Directory.Exists( path ) )
		//		return false;

		//	return path.ToLower().EndsWith( "." + extension.ToLower() );
		//}

		//[MenuItem( "Assets/[GTA3] Import DFF", true )]
		//public static bool ImportDFF_Valid()
		//{
		//	return CheckAssetFileExtension( Selection.activeObject, "dff" );
		//}

		//[MenuItem( "Assets/[GTA3] Import TXD", true )]
		//public static bool ImportTXD_Valid()
		//{
		//	return CheckAssetFileExtension( Selection.activeObject, "txd" );
		//}

		//[MenuItem( "Assets/[GTA3] Import IPL", true )]
		//public static bool ImportIPL_Valid()
		//{
		//	return CheckAssetFileExtension( Selection.activeObject, "ipl" );
		//}

		//[MenuItem( "Assets/[GTA3] Import IDE", true )]
		//public static bool ImportIDE_Valid()
		//{
		//	return CheckAssetFileExtension( Selection.activeObject, "ide" );
		//}

		//[MenuItem( "Assets/[GTA3] Import DFF", false )]
		//[MenuItem( "Assets/[GTA3] Import TXD", false )]
		//[MenuItem( "Assets/[GTA3] Import IPL", false )]
		//[MenuItem( "Assets/[GTA3] Import IDE", false )]
		//public static void Import()
		//{
		//	string path = AssetDatabase.GetAssetPath( Selection.activeObject );
		//	Importer imp = new Importer();
		//	imp.ImportFile( path );
		//}

		//--------------------------------------------------------------------------
		// EDITOR PROGRESS BAR
		//--------------------------------------------------------------------------

		public struct ProgressState
		{
			public string title;
			public string info;
			public float progress;
		}

		public static Stack<ProgressState> progressStack = new Stack<ProgressState>();
		public static float progressUpdateTime = 0f;

		public static void Progress_Start( string title )
		{
			ProgressState ps = new ProgressState();
			ps.title = title;
			ps.info = "";
			ps.progress = 0f;
			
			progressStack.Push( ps );
			Progress_Update( "", 0f );
		}

		public static void Progress_Stop()
		{
			if ( progressStack.Count == 0 )
				throw new UnityException( "Progress Bars Stack Underflow" );

			progressStack.Pop();
			
			if ( progressStack.Count > 0 )
			{
				ProgressState ps = progressStack.Peek();
				Progress_Update( ps.info, ps.progress );
			}
			else
			{
				EditorUtility.ClearProgressBar();
			}
		}

		public static bool Progress_Update( string info, float progress )
		{
			bool canceled = false;
			bool inProgress = progressStack.Count > 0;
			if ( inProgress && progress < 1f )
			{
				ProgressState ps = progressStack.Peek();
				ps.info = info;
				ps.progress = progress;

				float timeInterval = ( float ) EditorApplication.timeSinceStartup - progressUpdateTime;
				if ( timeInterval > 0.0025f )
				{
					progressUpdateTime = ( float ) EditorApplication.timeSinceStartup;
					canceled = EditorUtility.DisplayCancelableProgressBar( ps.title, ps.info, ps.progress );
					if ( canceled )
						Progress_Stop();
				}
			}
			return canceled;
		}
	}
}