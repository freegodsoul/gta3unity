﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace GTA3
{
	public partial class Importer /* .Cars */
	{
		public CarDef[] carDefs = { };
		public string[] carNames = { };
		public Dictionary<string, CarHandlingData> carHandlings = new Dictionary<string, CarHandlingData>();

		public void ImportHandling( string handlingName )
		{
			string path = GetSrcDataPath( SrcDataType.Config, handlingName );

			HandlingParser parser = new HandlingParser();
			parser.Execute( path );
			foreach ( CarHandlingData chd in parser.handlings )
				carHandlings[ chd.id ] = chd;
		}

		public void UpdateCarDefs()
		{
			int id_start = IDE.itemGroupIds[ ( int ) ItemGroup.Cars, 0 ];
			int id_end = IDE.itemGroupIds[ ( int ) ItemGroup.Cars, 1 ];

			int count = id_end - id_start + 1;
			carDefs = new CarDef[ count ];
			carNames = new string[ count ];

			for ( int i = 0; i < count; i++ )
			{
				int id = id_start + i;
				CarDef cardef = ( CarDef ) GetItemDef( id );
				carDefs[ i ] = cardef;
				carNames[ i ] = cardef.modelName;
			}

		}

		public Transform ImportCars( string colname )
		{
			Transform root_cars = new GameObject( "cars" ).transform;
			Transform root_cols = ImportCollisions( colname );


			int id_start = IDE.itemGroupIds[ ( int ) ItemGroup.Cars, 0 ];
			int id_end = IDE.itemGroupIds[ ( int ) ItemGroup.Cars, 1 ];

			float x = 0f;
			int i = 0;
			int count = id_end - id_start + 1;

			Progress_Start( "Importing Cars" );
			for ( int id = id_start; id <= id_end; id++ )
			{
				if ( Progress_Update( i + " / " + count, ( float ) i / ( float ) count ) )
					break;

				Car car = ( Car ) ImportItem( id, false, "", "" );
				car.transform.parent = root_cars;

				// pos correction
				const float spacing = 1f;
				Bounds bounds = car.transform.HierarchyBounds();
				x += bounds.extents.x;
				car.transform.Translate( x, 0f, 0f, Space.Self );
				x += bounds.extents.x + spacing;
				
				i++;
			}
			Progress_Stop();

			GameObject.DestroyImmediate( root_cols );
			return root_cars;
		}

		public Car Construct_Car( Frame carModel, CarDef cardef )
		{
			Car car = carModel.gameObject.AddComponent<Car>();
			car.cardef = cardef;

			//Transform wheelFrame = null;
			if ( cardef.carType == "car" )
			{
				ItemDef wheelDef = GetItemDef( cardef.wheelId );
				Object wheelPrefab = GetModelPrefab( wheelDef.modelName );

				Construct_Wheel( car, "wheel_lf_dummy", wheelPrefab, cardef.wheelScale, true );
				Construct_Wheel( car, "wheel_rf_dummy", wheelPrefab, cardef.wheelScale, false );
				Construct_Wheel( car, "wheel_lb_dummy", wheelPrefab, cardef.wheelScale, true );
				Construct_Wheel( car, "wheel_rb_dummy", wheelPrefab, cardef.wheelScale, false );
				Construct_Wheel( car, "wheel_lm_dummy", wheelPrefab, cardef.wheelScale, true );
				Construct_Wheel( car, "wheel_rm_dummy", wheelPrefab, cardef.wheelScale, false );
			}

			// Находим модель коллизии, если импортирована
			if ( collisions.ContainsKey( car.name ) )
			{
				Transform ct = collisions[ car.name ];
				ct.name = "collision_body";
				ct.parent = car.transform;
				ct.localPosition = Vector3.zero;
				ct.localRotation = Quaternion.identity;
				collisions.Remove( car.name );
			}

			if ( carHandlings.ContainsKey( cardef.handlingId ) )
			{
				car.handling = carHandlings[ cardef.handlingId ];
			}
			
			// Correct position
			Bounds bounds = car.transform.HierarchyBounds();
			float dy = bounds.center.y - bounds.extents.y;
			car.transform.Translate( 0f, -dy, 0f, Space.Self );

			return car;
		}

		public void Construct_Wheel( Car car, string wheelFrameName, Object wheelPrefab, float wheelScale, bool leftSide )
		{
			Transform wheelFrame = car.transform.FindChild( wheelFrameName );
			if ( wheelFrame )
			{
				Vector3 scale = Vector3.one * wheelScale;
				if ( leftSide ) scale.x = -scale.x;

				GameObject wheelGO = ( GameObject ) GameObject.Instantiate( wheelPrefab );
				wheelGO.name = wheelPrefab.name;
				wheelGO.transform.parent = wheelFrame;
				wheelGO.transform.localPosition = Vector3.zero;
				wheelGO.transform.localRotation = Quaternion.identity;
				wheelGO.transform.localScale = scale;
			}
		}

	}
}