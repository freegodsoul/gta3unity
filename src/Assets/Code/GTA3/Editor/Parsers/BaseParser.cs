﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GTA3
{
	public class BaseParser
	{
		public enum TokenType
		{
			TOK_EMPTY,			// пустой токен
			TOK_STRING,			// строка (имена файлов, объектов и т.д.)
			TOK_NUMBER,			// число (дробное или целое)
			TOK_COMMA,			// , запятая
			TOK_HASHSIGN,		// # comment
			TOK_SEMICOLON,		// ; точка с запятой
			
			TOK_SECTION_START,	// название одной из секций (напимер inst в IPL-файле или objs в IDE-файле)
			TOK_SECTION_END		// end
		}

		public class Token
		{
			public TokenType type;
			public int lineIndex;
			public int charIndex;
			public string text;
			public float number;
			public ItemSection sect;

			public Token( string text, int lineIndex, int charIndex )
			{
				this.text = text;
				this.lineIndex = lineIndex;
				this.charIndex = charIndex;
				Parse();
			}

			public TokenType Parse()
			{
				if ( text == "" )
					return type = TokenType.TOK_EMPTY;

				if ( text == "," )
					return type = TokenType.TOK_COMMA;

				if ( text == "#" )
					return type = TokenType.TOK_HASHSIGN;

				if ( text == ";" )
					return type = TokenType.TOK_SEMICOLON;

				if ( StringIsSection( text ) )
				{
					sect = StringToSection( text );
					return type = TokenType.TOK_SECTION_START;
				}

				if ( text == "end" )
					return type = TokenType.TOK_SECTION_END;

				if ( float.TryParse( text, out number ) )
					return type = TokenType.TOK_NUMBER;

				return type = TokenType.TOK_STRING;
			}
		}

		public static readonly string[] sectionToString = new string[]
		{
			// IPL
			"inst",
			"cull",
			"pick",
			
			// IDE
			"objs",
			"tobj",
			"peds",
			"cars",
			"hier",
			"2dfx",
			"path"
		};

		public string filepath;
		public string filename;

		protected StreamReader reader;
		protected string currentLine;
		protected int currentLineIndex;
		protected int currentCharIndex;

		protected List<Token> tokenList;
		protected int currentTokenIndex;

		// progress bar
		protected virtual string progressTitle { get { return "ParserBase.progressTitle"; } }
		protected string progressInfo { get { return "Tokens: " + currentTokenIndex + "/" + tokenList.Count; } }
		protected float progressRatio { get { return ( float ) currentTokenIndex / ( float ) tokenList.Count; } }

		public virtual void Execute( string path )
		{
			Log.WriteLine( path );

			filepath = path;
			filename = Path.GetFileName( path );

			Tokenize( path );

			Importer.Progress_Start( progressTitle );
			for ( currentTokenIndex = 0; currentTokenIndex < tokenList.Count; currentTokenIndex++ )
			{
				if ( Importer.Progress_Update( progressInfo, progressRatio ) )
					break;

				Token token = tokenList[ currentTokenIndex ];
				Log.WriteLine( "[" + token.type + "] " + token.text );

				if ( token.type == TokenType.TOK_SECTION_START )
				{
					currentTokenIndex++;
					ExecuteSection( token.sect );
				}
			}
			Importer.Progress_Stop();
		}

		public virtual void ExecuteSection( ItemSection sect ) { }

		public void Tokenize( string path )
		{
			reader = new StreamReader( path, Encoding.UTF8 );
			if ( !reader.BaseStream.CanRead )
			{
				Debug.LogError( "Не удалось считать файл\n" + path );
				return;
			}

			currentLineIndex = 0;
			currentLine = "";
			currentCharIndex = 0;

			tokenList = new List<Token>();

			while ( true )
			{
				Token token = Tokenize_NextToken();

				// Конец файла
				if ( token == null )
					break;

				// Пропускаем комментарии
				if ( token.type == TokenType.TOK_HASHSIGN || token.type == TokenType.TOK_SEMICOLON )
				{
					currentCharIndex = currentLine.Length;
					continue;
				}

				// Пропускаем запятые
				if ( token.type == TokenType.TOK_COMMA )
					continue;

				// Пропускаем пустое пространство
				if ( token.type == TokenType.TOK_EMPTY )
					continue;

				tokenList.Add( token );
			}

			reader.Close();
		}

		public Token Tokenize_NextToken()
		{
			while ( true )
			{
				if ( currentCharIndex == currentLine.Length )
				{
					if ( reader.EndOfStream )
					{
						//LogUtils.WriteLine( "reader.EndOfStream" );
						//LogUtils.WriteLine( "currentChar: " + currentChar );
						//LogUtils.WriteLine( "currentLine.Length: " + currentLine.Length );
						return null;
					}

					currentLine = reader.ReadLine();
					currentCharIndex = 0;
					currentLineIndex++;
					Log.WriteLine( "[LINE: " + currentLineIndex + " line, " + currentLine.Length + " chars] " + currentLine );
				}

				Token token = Tokenize_NextTokenInLine();
				Log.WriteLine( "[" + token.type + "] " + token.text );

				return token;
			}
		}

		public Token Tokenize_NextTokenInLine()
		{
			string tokenstr = "";

			// Пропускаем white spaces
			while ( currentCharIndex != currentLine.Length && char.IsWhiteSpace( currentLine[ currentCharIndex ] ) )
				currentCharIndex++;

			while ( currentCharIndex != currentLine.Length )
			{
				char ch = currentLine[ currentCharIndex ];

				// Конец токена
				if ( char.IsWhiteSpace( ch ) )
					break;

				// Конец токена
				if ( tokenstr.Length > 0 )
					if ( ch == ',' || ch == '#' )
						break;

				tokenstr += ch;
				currentCharIndex++;

				// comma
				if ( ch == ',' )
					break;

				// hash sign
				if ( ch == '#' )
					break;

				// semicolon
				if ( ch == ';' )
					break;
			}

			return new Token( tokenstr, currentLineIndex, currentCharIndex );
		}

		public string ParseString()
		{
			return ParseToken( TokenType.TOK_STRING ).text;
		}

		public float ParseFloat()
		{
			return ParseToken( TokenType.TOK_NUMBER ).number;
		}

		public int ParseInteger()
		{
			return ( int ) ParseToken( TokenType.TOK_NUMBER ).number;
		}

		public int ParseHex()
		{
			Token token = ParseToken( TokenType.TOK_NUMBER, TokenType.TOK_STRING );
			return System.Convert.ToInt32( token.text, 16 );
		}

		public Vector3 ParseFloat3()
		{
			Vector3 v = new Vector3();
			v.x = ParseFloat();
			v.y = ParseFloat();
			v.z = ParseFloat();
			return v;
		}

		public Quaternion ParseQuaternion()
		{
			Quaternion q = new Quaternion();
			q.x = ParseFloat();
			q.y = ParseFloat();
			q.z = ParseFloat();
			q.w = ParseFloat();
			return q;
		}

		public Token ParseToken( params TokenType[] expected )
		{
			Token token = tokenList[ currentTokenIndex ];

			bool correct = false;
			foreach ( TokenType type in expected )
			{
				if ( type == token.type || type == TokenType.TOK_STRING )
				{
					correct = true;
					break;
				}
			}

			if ( !correct )
			{
				throw new UnityException(
					"Unexpected token: " + token.type +
					", expected tokens: " + expected.JoinToString() + 
					"\n" + filename + 
					", line: " + token.lineIndex +
					", char: " + token.charIndex );
			}
			
			currentTokenIndex++;
			return token;
		}

		public static bool StringIsSection( string text )
		{
			return sectionToString.IndexOf( text ) != -1;
		}

		public static ItemSection StringToSection( string s )
		{
			return ( ItemSection ) sectionToString.IndexOf( s );
		}

		public static string SectionToString( ItemSection s )
		{
			return sectionToString[ ( int ) s ];
		}
	}

}