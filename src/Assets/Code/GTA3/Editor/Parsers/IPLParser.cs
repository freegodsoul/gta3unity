﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GTA3
{
	public class IPLParser : BaseParser
	{
		public List<ItemPlacement> placements;

		protected override string progressTitle { get { return "IPL parsing: " + filepath; } }

		public IPLParser()
		{
			placements = new List<ItemPlacement>();
		}

		public override void Execute( string path )
		{
			placements.Clear();
			base.Execute( path );
		}

		public override void ExecuteSection( ItemSection sect )
		{
			while ( tokenList[ currentTokenIndex ].type != TokenType.TOK_SECTION_END )
			{
				//if ( Importer.Progress_Update( progressInfo, progressRatio ) )
				//	break;

				switch ( sect )
				{
					case ItemSection.SEC_INST:
						ItemPlacement pl = ParseItemPlacement( sect );
						placements.Add( pl );
						break;

					default:
						currentTokenIndex++;
						break;
				}
			}
		}

		public ItemPlacement ParseItemPlacement( ItemSection sect )
		{
			ItemPlacement pl = new ItemPlacement();
			pl.type = sect;

			switch ( sect )
			{
				case ItemSection.SEC_INST:
					pl.id = ParseInteger();
					/*pl.modelName = */ParseString();
					pl.position = ParseFloat3();
					pl.scale = ParseFloat3();
					pl.rotation = ParseQuaternion();
					break;

				case ItemSection.SEC_CULL:
				case ItemSection.SEC_PICK:
					currentTokenIndex++;
					break;
			}

			return pl;
		}
	}

}