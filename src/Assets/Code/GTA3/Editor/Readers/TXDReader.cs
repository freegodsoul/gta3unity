﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Text;

namespace GTA3
{
	public class TXDReader : RWSReader
	{
		public Texture[] textures;
		public FilterMode textureFilter;

		public TXDReader( string path, FilterMode textureFilter )
			: base( path )
		{
			this.textureFilter = textureFilter;
		}

		public void Read()
		{
			//	* Texture Dictionary
			//		 o Struct - 2b - количество текстур в архиве, 1b - deviceId (обычно 0), 1b - 0.
			//		 o Texture Native - данная секция в TXD может повторяться несколько раз, в зависимости от количества текстур в архиве.
			//			   + Struct - заголовок с именем и параметрами текстуры + тело текстуры
			//			   + Extension - дополнение подсекции (не используется) 
			//		 o Extension - дополнение секции (не используется)

			// [TexDict]
			RwsSectionHeader h_txd = OpenSection( RwsSectionID.ID_TEXDICT );

			if ( h_txd.version != 0x0800FFFF )
			{
				Log.WriteLine( "Неподдерживаемая версия txd секции" );
				Debug.LogError( filepath + "\nНеподдерживаемая версия txd секции: " + VersionToString( h_txd.version ) );
				CloseSection( h_txd );
				return;
			}

			// [TexDict] -> [Struct]
			RwsSectionHeader h_txdstruct = OpenSection( RwsSectionID.ID_STRUCT );
			short count = reader.ReadInt16();
			short unused = reader.ReadInt16();
			Log.WriteLine( prefix + "count: " + count );
			Log.WriteLine( prefix + "unused: " + unused );
			CloseSection( h_txdstruct );

			// [TexDict] -> [TexNative] {count}
			textures = new Texture[ count ];
			for ( int i = 0; i < count; i++ )
			{
				// [TexDict] -> [TexNative] 
				RwsSectionHeader h_tex = OpenSection( RwsSectionID.ID_TEXNATIVE );

				// [TexDict] -> [TexNative] -> [Struct]
				textures[ i ] = ReadTextureNativeStruct();

				// [TexDict] -> [TexNative] -> [Extension]
				RwsSectionHeader h_texext = OpenSection( RwsSectionID.ID_EXTENSION );
				CloseSection( h_texext );

				CloseSection( h_tex );
			}

			// [TexDict] -> [Extension]
			RwsSectionHeader h_ext = OpenSection( RwsSectionID.ID_EXTENSION );
			CloseSection( h_ext );

			CloseSection( h_txd );
		}

		public Texture ReadTextureNativeStruct()
		{
			RwsSectionHeader h_texstruct = OpenSection( RwsSectionID.ID_STRUCT );

			RwsTextureNativeHeader h = ReadTextureNativeHeader();
			PrintTexNativeHeader( h );

			if ( h.platformID != 8 )
				throw new UnityException( "PlatformID не является 8" );

			if ( h.compression != 0 )
				throw new UnityException( "Компрессия не поддерживается" );

			RwsRasterFormat format = ( RwsRasterFormat ) h.rasterFormat & RwsRasterFormat.RF_MASK_FORMAT;
			RwsRasterFormat flags = ( RwsRasterFormat ) h.rasterFormat & RwsRasterFormat.RF_MASK_FLAGS;

			if ( ( flags & RwsRasterFormat.RF_FLAG_PAL4 ) == RwsRasterFormat.RF_FLAG_PAL4 )
				throw new UnityException( "Неподдерживаемый формат: " + RasterFormatToString( ( RwsRasterFormat ) h.rasterFormat ) );

			bool transparent = h.hasAlpha != 0;
			bool hasPalette = ( flags & RwsRasterFormat.RF_FLAG_PAL8 ) == RwsRasterFormat.RF_FLAG_PAL8;

			byte[] palette = null;
			if ( hasPalette )
			{
				int palette_size = PAL8_COLORS * PAL8_CHANNELS;
				palette = new byte[ palette_size ];
				reader.Read( palette, 0, palette_size );
				//Texture texpal = CreateTexture( 16, 16, 32, true, false, false, palette );
				//UnityUtils.SaveTextureToFile( texpal, "Textures/" + txdname + "/" + h.name + "_pal.png" );
			}

			uint raster_size = reader.ReadUInt32();
			Log.WriteLine( prefix + "raster size: " + raster_size );

			int pixels_channels = ( h.bpp / 8 );
			int pixels_size = h.width * h.height * pixels_channels;
			byte[] pixels = new byte[ pixels_size ];
			reader.Read( pixels, 0, pixels_size );

			CloseSection( h_texstruct );

			// Создаем и возвращаем текстуру
			Texture tex = CreateTexture( h.width, h.height, h.bpp, transparent, textureFilter, ( RwsRasterFormat ) h.rasterFormat, pixels, palette );
			tex.name = h.name;
			return tex;
		}

		public RwsTextureNativeHeader ReadTextureNativeHeader()
		{
			RwsTextureNativeHeader h = new RwsTextureNativeHeader();
			h.platformID = reader.ReadUInt32();
			h.filterFlags = reader.ReadUInt16();
			h.wrapModeU = reader.ReadByte();
			h.wrapModeV = reader.ReadByte();
			h.name = ReadString();
			h.maskName = ReadString();
			h.rasterFormat = reader.ReadUInt32();
			h.hasAlpha = reader.ReadUInt32();
			h.width = reader.ReadUInt16();
			h.height = reader.ReadUInt16();
			h.bpp = reader.ReadByte();
			h.mipmapLevels = reader.ReadByte();
			h.rasterType = reader.ReadByte();
			h.compression = reader.ReadByte();
			return h;
		}

		public void PrintTexNativeHeader( RwsTextureNativeHeader h )
		{
			Log.WriteLine( prefix + "platformID: " + h.platformID );
			Log.WriteLine( prefix + "filterFlags: " + h.filterFlags );
			Log.WriteLine( prefix + "wrapModeU: " + h.wrapModeU );
			Log.WriteLine( prefix + "wrapModeV: " + h.wrapModeV );
			Log.WriteLine( prefix + "name: " + h.name );
			Log.WriteLine( prefix + "maskName: " + h.maskName );
			Log.WriteLine( prefix + "rasterFormat: " + RasterFormatToString( ( RwsRasterFormat ) h.rasterFormat ) );
			Log.WriteLine( prefix + "hasAlpha: " + ( h.hasAlpha != 0 ) );
			Log.WriteLine( prefix + "width: " + h.width );
			Log.WriteLine( prefix + "height: " + h.height );
			Log.WriteLine( prefix + "bpp: " + h.bpp );
			Log.WriteLine( prefix + "mipmapLevels: " + h.mipmapLevels );
			Log.WriteLine( prefix + "rasterType: " + h.rasterType );
			Log.WriteLine( prefix + "compression: " + h.compression );
		}


		public static string RasterFormatToString( RwsRasterFormat rf )
		{
			RwsRasterFormat format = rf & RwsRasterFormat.RF_MASK_FORMAT;
			RwsRasterFormat flags = rf & RwsRasterFormat.RF_MASK_FLAGS;

			return format + " [ " + flags.FlagsToString() + " ]";
		}

		public static Texture CreateTexture( int w, int h, int bpp, bool transparent, FilterMode textureFilter, RwsRasterFormat rf, byte[] pixels, byte[] palette )
		{
			RwsRasterFormat format = rf & RwsRasterFormat.RF_MASK_FORMAT;
			RwsRasterFormat flags = rf & RwsRasterFormat.RF_MASK_FLAGS;

			Texture2D tex;
			if ( transparent )
			{
				tex = new Texture2D( w, h, TextureFormat.RGBA32, false, false );
#if UNITY_EDITOR
				tex.alphaIsTransparency = true;
#endif
			}
			else
			{
				tex = new Texture2D( w, h, TextureFormat.RGB24, false, false );
			}
			tex.filterMode = textureFilter;

			int bytesPerPixel = bpp / 8;
			int bytesPerColor = 4; // 32-bit RGBA palette
			float byteToFloat = 1f / 255f;
			float bit5ToFloat = 1f / 31f;

			for ( int i = 0; i < h; i++ )
			{
				int y = h - i - 1;
				for ( int j = 0; j < w; j++ )
				{
					int pixelIndex = ( j + i * w );
					float r, g, b, a;

					if ( palette != null )
					{
						int colorIndex = pixels[ pixelIndex ];
						int colorOffset = colorIndex * bytesPerColor;

						r = palette[ colorOffset + 0 ] * byteToFloat;
						g = palette[ colorOffset + 1 ] * byteToFloat;
						b = palette[ colorOffset + 2 ] * byteToFloat;
						a = transparent ? palette[ colorOffset + 3 ] : 1f;
					}
					else
					{
						int pixelOffset = pixelIndex * bytesPerPixel;
						switch ( format )
						{
							case RwsRasterFormat.RF_888:
							case RwsRasterFormat.RF_8888:
								r = pixels[ pixelOffset + 2 ] * byteToFloat;
								g = pixels[ pixelOffset + 1 ] * byteToFloat;
								b = pixels[ pixelOffset + 0 ] * byteToFloat;
								a = transparent ? pixels[ pixelOffset + 3 ] * byteToFloat : 1f;
								break;

							case RwsRasterFormat.RF_1555:
								short pixdata = BitConverter.ToInt16( pixels, pixelOffset );
								r = ( float ) ( ( pixdata >> 10 ) & 0x1F ) * bit5ToFloat;
								g = ( float ) ( ( pixdata >> 5 ) & 0x1F ) * bit5ToFloat;
								b = ( float ) ( ( pixdata >> 0 ) & 0x1F ) * bit5ToFloat;
								a = ( float ) ( ( pixdata >> 15 ) & 0x01 );
								break;

							default:
								throw new UnityException( "Неподдерживаемый формат: " + format + " [ " + flags.FlagsToString() + " ]" );
						}
					}

					Color c = new Color(r,g,b,a);
					tex.SetPixel( j, y, c );
				}
			}
			tex.Apply();
			return tex;
		}

		public static Texture CreateTexture_8888( int w, int h, int bpp, bool transparent, FilterMode textureFilter, byte[] pixels )
		{
			Texture2D tex;
			if ( transparent )
				tex = new Texture2D( w, h, TextureFormat.RGBA32, false, false );
			else
				tex = new Texture2D( w, h, TextureFormat.RGB24, false, false );
			tex.filterMode = textureFilter;

			int bytesPerPixel = bpp / 8;
			float byteToFloat = 1f / 255f;

			for ( int i = 0; i < h; i++ )
			{
				int y = h - i - 1;
				for ( int j = 0; j < w; j++ )
				{
					int pixelIndex = ( j + i * w );
					int pixelOffset = pixelIndex * bytesPerPixel;

					byte r = pixels[ pixelOffset + 2 ];
					byte g = pixels[ pixelOffset + 1 ];
					byte b = pixels[ pixelOffset + 0 ];
					byte a = transparent ? pixels[ pixelOffset + 3 ] : ( byte ) 255;

					Color c = new Color(
						( float ) r * byteToFloat,
						( float ) g * byteToFloat,
						( float ) b * byteToFloat,
						( float ) a * byteToFloat );

					tex.SetPixel( j, y, c );
				}
			}
			tex.Apply();
			return tex;
		}

		public static Texture CreateTexture_8888_PAL8( int w, int h, int bpp, bool transparent, FilterMode textureFilter, byte[] pixels, byte[] palette )
		{
			Texture2D tex;
			if ( transparent )
				tex = new Texture2D( w, h, TextureFormat.RGBA32, false, false );
			else
				tex = new Texture2D( w, h, TextureFormat.RGB24, false, false );
			tex.filterMode = textureFilter;

			int bytesPerColor = 4; // RGBA palette
			float byteToFloat = 1f / 255f;

			for ( int i = 0; i < h; i++ )
			{
				int y = h - i - 1;
				for ( int j = 0; j < w; j++ )
				{
					int pixelIndex = ( j + i * w );
					int colorIndex = pixels[ pixelIndex ];
					int colorOffset = colorIndex * bytesPerColor;

					byte r = palette[ colorOffset + 0 ];
					byte g = palette[ colorOffset + 1 ];
					byte b = palette[ colorOffset + 2 ];
					byte a = transparent ? palette[ colorOffset + 3 ] : ( byte ) 255;

					Color c = new Color(
						( float ) r * byteToFloat,
						( float ) g * byteToFloat,
						( float ) b * byteToFloat,
						( float ) a * byteToFloat );

					tex.SetPixel( j, y, c );
				}
			}
			tex.Apply();
			return tex;
		}
	}
}

