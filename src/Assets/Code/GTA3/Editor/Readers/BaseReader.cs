﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Text;

namespace GTA3
{
	public class BaseReader
	{
		public string filepath;
		public BinaryReader reader;
		public string prefix;

		public bool canRead { get { return reader.BaseStream.CanRead; } }
		public string filename { get { return Path.GetFileNameWithoutExtension( filepath ); } }

		public BaseReader( string path )
		{
			filepath = path;
			prefix = "";
			
			Begin( path );
		}

		public bool Begin( string path )
		{
			Print( path );

			if ( path == "" )
				return false;

			FileStream stream = new FileStream( path, FileMode.Open, FileAccess.Read );
			if ( !stream.CanRead )
			{
				Debug.LogError( "Не удается открыть/прочитать файл " + path );
				return false;
			}

			reader = new BinaryReader( stream, Encoding.Default );
			return true;
		}

		public bool End()
		{
			reader.Close();
			return true;
		}

		public bool Error( string err )
		{
			Debug.LogError( err );
			reader.Close();
			return false;
		}

		public void PrefixIncr()
		{
			prefix += '\t';
		}

		public void PrefixDecr()
		{
			prefix = prefix.Decrement();
		}

		public void Print( string s )
		{
			Log.WriteLine( prefix + s );
		}

		public Vector2 ReadVector2()
		{
			Vector2 v = new Vector2();
			v.x = reader.ReadSingle();
			v.y = reader.ReadSingle();
			return v;
		}

		public Vector3 ReadVector3()
		{
			Vector3 v = new Vector3();
			v.x = reader.ReadSingle();
			v.y = reader.ReadSingle();
			v.z = reader.ReadSingle();
			return v;
		}

		public Vector4 ReadVector4()
		{
			Vector4 v = new Vector4();
			v.x = reader.ReadSingle();
			v.y = reader.ReadSingle();
			v.z = reader.ReadSingle();
			v.w = reader.ReadSingle();
			return v;
		}

		public Quaternion ReadQuaternion()
		{
			Quaternion q = new Quaternion();
			q.x = reader.ReadSingle();
			q.y = reader.ReadSingle();
			q.z = reader.ReadSingle();
			q.w = reader.ReadSingle();
			return q;
		}

		public Color32 ReadColor32()
		{
			Color32 c = new Color32();
			c.r = reader.ReadByte();
			c.g = reader.ReadByte();
			c.b = reader.ReadByte();
			c.a = reader.ReadByte();
			return c;
		}

		public string ReadString( int size )
		{
			string s = new string( reader.ReadChars( size ) );
			int nul = s.IndexOf( '\0' );
			if ( nul != -1 )
				return s.Substring( 0, nul );
			return s;
		}

		// UTILS

		public static uint HiByte( short s )
		{
			return ( uint ) s >> 16;
		}

		public static uint LoByte( short s )
		{
			return ( uint ) s;
		}

		public static uint HiNibble( byte b )
		{
			return ( uint ) b >> 4;
		}

		public static uint LoNibble( byte b )
		{
			return ( uint ) b;
		}

		public static string VersionToString( int version )
		{
			byte b1 = ( byte ) ( ( version >> 24 ) & 0xFF );
			byte b2 = ( byte ) ( ( version >> 16 ) & 0xFF );
			byte b3 = ( byte ) ( ( version >> 8 ) & 0xFF );
			byte b4 = ( byte ) ( ( version >> 0 ) & 0xFF );
			return b1 + "." + b2 + "." + b3 + "." + b4;
		}
	}
}
