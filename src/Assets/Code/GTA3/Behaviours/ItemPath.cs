﻿using UnityEngine;
using System.Collections;

namespace GTA3
{
	public enum PathType
	{
		Car,
		Ped
	}

	public class ItemPath : MonoBehaviour
	{
		public float gizmoScale = 1f / 16f;
		public string type;
		public PathDefNode[] nodes;

		// Use this for initialization
		void Start()
		{
		}

		// Update is called once per frame
		void Update()
		{
		}

		void OnDrawGizmos()
		{
			Gizmos.matrix = transform.localToWorldMatrix * Matrix4x4.Scale( Vector3.one * gizmoScale );
			Gizmos.color = type == "car" ? Color.green : Color.blue;

			for ( int i = 0; i < nodes.Length; i++ )
			{
				PathDefNode node = nodes[ i ];
				if ( node.type == PathDefNodeType.None )
					break;

				// position
				Gizmos.DrawWireCube( node.position, Vector3.one );

				// link
				if ( node.type == PathDefNodeType.Internal )
				{
					if ( node.linkTo != -1 )
					{
						PathDefNode linknode = nodes[ node.linkTo ];
						Gizmos.DrawLine( node.position, linknode.position );
					}
				}
			}
		}
	}

}