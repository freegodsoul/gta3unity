﻿using UnityEngine;
using System.Collections.Generic;

namespace GTA3
{
	public enum WheelType
	{
		Front, Rear, Middle
	}

	public class Wheel
	{
		public WheelType type;			// wheel type
		public Transform dummy;			// WheelCollider.transform
		public WheelCollider wc;		// WheelCollider
		public Transform wt;			// визуальная часть колеса
		public float rotation;			// rotation in degrees

		public bool isSteering { get { return type == WheelType.Front; } }
	}

	public enum DriveType
	{
		FWD, RWD, FOUR
	}

	public enum EngineType
	{
		Petrol, Diesel, Electric
	}

	public enum LightType
	{
		Long, Small, Big, Tall
	}

	public enum CarFlags
	{
		//;		1st digit =		1: 1G_BOOST         2: 2G_BOOST       4: REV_BONNET    8: HANGING_BOOT
		//;		2nd digit =		1: NO_DOORS	        2: IS_VAN	      4: IS_BUS        8: IS_LOW
		//;		3rd digit =	 	1: DBL_EXHAUST	    2: TAILGATE_BOOT  4: NOSWING_BOOT  8: NONPLAYER_STABILISER  
		//;		4th digit =		1: NEUTRALHANDLING  2: HAS_NO_ROOF    4: IS_BIG        8: HALOGEN_LIGHTS

		BOOST_1G = 0x0001,
		BOOST_2G = 0x0002,
		REV_BONNET = 0x0004,
		HANGING_BOOT = 0x0008,

		NO_DOORS = 0x0010,
		IS_VAN = 0x0020,
		IS_BUS = 0x0040,
		IS_LOW = 0x0080,

		DBL_EXHAUST = 0x0100,
		TAILGATE_BOOT = 0x0200,
		NOSWING_BOOT = 0x0400,
		NONPLAYER_STABILISER = 0x0800,

		NEUTRALHANDLING = 0x1000,
		HAS_NO_ROOF = 0x2000,
		IS_BIG = 0x4000,
		HALOGEN_LIGHTS = 0x8000
	}

	[System.Serializable]
	public struct TransmissionData
	{
		public int numGears;
		public float maxVelocity;
		public float engineAcceleration;
		public DriveType driveType;
		public EngineType engineType;
	}

	[System.Serializable]
	public class CarHandlingData
	{
		public string id;								// name of car
		public float mass;								// Vehicle weight (Kg)
		public Vector3 dimensions;						// 
		public Vector3 com;								// Center of Mass. Relative to car origin
		public float submergedRatio;					// how much the vehicle submerge (%) [10 to 120]

		// Traction
		public float tractionMultiplier;
		public float tractionLoss;
		public float tractionBias;

		// Transmission
		public TransmissionData transmission;

		// Brake
		public float brakeDeceleration;
		public float brakeBias;

		//
		public bool bABS;
		public float steeringLock;
		public float seatOffsetDistance;
		public float collisionDamageMultiplier;
		public int monetaryValue;

		// Suspension
		public float suspensionForceLevel;
		public float suspensionDampingLevel;
		public float suspensionUpperLimit;
		public float suspensionLowerLimit;
		public float suspensionBias;					// Ratio of suspension force to apply at the rear compared to the front.

		// Flags
		public CarFlags flags;

		// Lights
		public LightType frontLights;
		public LightType rearLights;
	}

	[System.Serializable]
	public class CarHandlingTweakParams
	{
		public float driveForceMultiplier = 0.05f;
		public float brakeForceMultiplier = 0.5f;
		//public float stabilizerAssistance = 0.25f;
		public float comHeightCoeff = 0.5f;
		
		public WheelFrictionCurve forwardFC = new WheelFrictionCurve()
		{
			extremumValue = 2000f,
			asymptoteValue = 1000f,
			stiffness = 1f
		};
		
		public WheelFrictionCurve sidewaysFC = new WheelFrictionCurve()
		{
			extremumValue = 1500f,
			asymptoteValue = 1000f,
			stiffness = 0.3f
		};
	}

	public class Car : Item
	{
		public const float RPM_2_DEG_PER_SEC = 360f / 60f;
		public const float MAX_DT = 1f / 20f;
		public const float WHEEL_LERP_SPEED = 4f;
		public const float STEER_LERP_SPEED = 8f;

		public CarDef cardef;
		public CarHandlingData handling;
		public List<Wheel> wheels;

		private Wheel wheel_lf;
		private Wheel wheel_rf;
		private Wheel wheel_lb;
		private Wheel wheel_rb;
		private Wheel wheel_lm;
		private Wheel wheel_rm;

		private CarHandlingTweakParams tweakParams = new CarHandlingTweakParams();

		// Use this for initialization
		void Awake()
		{
			// Rigidbody
			//-------------------------------------------------

			gameObject.AddComponent<Rigidbody>();
			rigidbody.interpolation = RigidbodyInterpolation.Interpolate;

			if ( cardef.carType == "car" || cardef.carType == "truck" )
			{
				// Wheels
				//-------------------------------------------------

				wheels = new List<Wheel>();

				wheel_lf = InitWheel( "wheel_lf_dummy", WheelType.Front );
				wheel_rf = InitWheel( "wheel_rf_dummy", WheelType.Front );
				wheel_lb = InitWheel( "wheel_lb_dummy", WheelType.Rear );
				wheel_rb = InitWheel( "wheel_rb_dummy", WheelType.Rear );
				wheel_lm = InitWheel( "wheel_lm_dummy", WheelType.Middle );
				wheel_rm = InitWheel( "wheel_rm_dummy", WheelType.Middle );

				// AntiRollBar
				//-------------------------------------------------

				AntiRollBar arb = gameObject.AddComponent<AntiRollBar>();
				arb.wheelL = wheel_lb.wc;
				arb.wheelR = wheel_rb.wc;

				arb = gameObject.AddComponent<AntiRollBar>();
				arb.wheelL = wheel_lf.wc;
				arb.wheelR = wheel_rf.wc;
			}
		}

		void Start()
		{
			if ( handling != null )
			{
				rigidbody.mass = handling.mass;
				Vector3 com = MathUtils.LeftHand( handling.com );

				if ( cardef.carType == "car" || cardef.carType == "truck" )
				{
					float wheel_h = wheel_lf.dummy.localPosition.y;
					com.y = Mathf.Lerp( wheel_h, com.y, tweakParams.comHeightCoeff );
				}

				rigidbody.centerOfMass = com;
			}
		}

		// Update is called once per frame
		void Update()
		{
			float dt = Mathf.Min( Time.deltaTime, MAX_DT );

			if ( cardef.carType == "car" || cardef.carType == "truck" )
			{
				// WHEELS
				foreach ( Wheel w in wheels )
				{
					AlignWheel( w, dt );
				}
			}
		}

		void OnGUI()
		{
			GUILayout.Label( "stabRollDeg: " + stabRollDeg );
		}

		void FixedUpdate()
		{
			float dt = Time.fixedDeltaTime;
			if ( cardef.carType == "car" || cardef.carType == "truck" )
			{
				Steering( dt );
				Engine( dt );
				Braking( dt );
				Drag( dt );
				//StabilizeRoll( dt );
			}
		}

		private float stabRollDeg = 0f;

		public void StabilizeRoll( float dt )
		{
			Vector3 groundNormal = Vector3.zero;
			int groundedCount = 0;

			foreach ( Wheel w in wheels )
			{
				WheelHit hit;
				if ( w.wc.GetGroundHit(out hit))
				{
					Debug.DrawRay( hit.point, hit.normal * 5f, Color.red );
					groundNormal += hit.normal;
					groundedCount++;
				}
			}

			if ( groundedCount < 2 )
				return;

			groundNormal /= ( float ) groundedCount;
			stabRollDeg = MathUtils.SignedAngle( groundNormal, transform.up, transform.forward ) * Mathf.Rad2Deg;
			const float maxAcceleration = 5f;
			Vector3 torque = -transform.forward * stabRollDeg * maxAcceleration;
			rigidbody.AddTorque( torque, ForceMode.Acceleration );
		}

		public void Steering( float dt )
		{
			float targetSteerAngle = Input.GetAxis( "Horizontal" ) * handling.steeringLock;

			foreach ( Wheel w in wheels )
			{
				if ( w.type == WheelType.Front )
				{
					w.wc.steerAngle = Mathf.Lerp( w.wc.steerAngle, targetSteerAngle, dt * STEER_LERP_SPEED );
				}
			}
		}

		public void Engine( float dt )
		{
			// Input

			float steerInput = Input.GetAxis( "Horizontal" ) * handling.steeringLock;
			float throttle = Input.GetAxis( "Vertical" );
			//throttle = Mathf.Clamp01( throttle );

			// Engine Torque - Рассчитываем крутящий момент двигателя

			float maxDriveForce = handling.mass * handling.transmission.engineAcceleration / ( float ) wheels.Count * tweakParams.driveForceMultiplier;
			float driveForce = throttle * maxDriveForce;

			// Stabilizer

			//float force = handling.mass * tweakParams.stabilizerAssistance;
			//float accel = throttle * steerInput;
			//rigidbody.AddRelativeTorque( Vector3.up * ( accel * force ) );

			// Wheel Torque

			foreach ( Wheel w in wheels )
			{
				if ( IsWheelDrivable( w ) )
				{
					float motorTorque = driveForce * w.wc.radius;
					w.wc.motorTorque = motorTorque;
				}
				else
				{
					w.wc.motorTorque = 0f;
				}
			}
		}

		public void Braking( float dt )
		{
			float handbrake = Input.GetAxis( "Handbrake" );

			float maxBrakeForce = handling.mass * handling.brakeDeceleration / ( float ) wheels.Count * tweakParams.brakeForceMultiplier;
			float brakeForce = handbrake * maxBrakeForce;

			// Wheel Torque

			foreach ( Wheel w in wheels )
			{
				if ( IsWheelDrivable(w) )
				{
					float brakeTorque = brakeForce * w.wc.radius;
					w.wc.brakeTorque = brakeTorque;
				}
				else
				{
					w.wc.brakeTorque = 0f;
				}
			}
		}

		public void Drag( float dt )
		{
		}

		public bool IsWheelDrivable( Wheel w )
		{
			if ( handling.transmission.driveType == DriveType.FOUR )
				return true;

			if ( handling.transmission.driveType == DriveType.FWD && w.type == WheelType.Front )
				return true;

			if ( handling.transmission.driveType == DriveType.RWD && w.type == WheelType.Rear )
				return true;

			if ( handling.transmission.driveType == DriveType.RWD && w.type == WheelType.Middle )
				return true;

			return false;
		}

		public Wheel InitWheel( string wheelDummyName, WheelType type )
		{
			Transform dummy = transform.FindChild( wheelDummyName );
			if ( dummy )
			{
				Wheel w = new Wheel();
				w.type = type;
				w.dummy = dummy;
				w.wc = dummy.gameObject.AddComponent<WheelCollider>();
				w.wt = dummy.GetChild( 0 );
				w.rotation = 0f;

				// WHEEL COLLIDER

				// Radius
				Bounds wb = w.wt.GetComponentInChildren<MeshFilter>().mesh.bounds;
				w.wc.radius = wb.extents.y * w.wt.transform.localScale.y;

				// Suspension
				float suspensionDistance = handling.suspensionUpperLimit - handling.suspensionLowerLimit;
				w.wc.suspensionDistance = suspensionDistance * cardef.wheelScale;
				w.wc.center = new Vector3( 0f, handling.suspensionUpperLimit, 0f ) * cardef.wheelScale;

				// Basic suspension spring force = weight = mg
				float suspensionSpringForce = handling.mass * Physics.gravity.magnitude;
				JointSpring js = w.wc.suspensionSpring;
				js.spring = handling.suspensionForceLevel * suspensionSpringForce;
				js.damper = handling.suspensionDampingLevel;
				w.wc.suspensionSpring = js;

				// Traction
				float tractionMultiplier = handling.tractionMultiplier;
				if ( type == WheelType.Front )
					tractionMultiplier *= handling.tractionBias;
				else
					tractionMultiplier *= 1f - handling.tractionBias;

				WheelFrictionCurve fc = w.wc.forwardFriction;
				fc.extremumValue = tweakParams.forwardFC.extremumValue;
				fc.asymptoteValue = tweakParams.forwardFC.asymptoteValue;
				fc.stiffness = tweakParams.forwardFC.stiffness;
				w.wc.forwardFriction = fc;

				// Sideways Friction
				fc = w.wc.sidewaysFriction;
				fc.extremumValue = tweakParams.sidewaysFC.extremumValue;
				fc.asymptoteValue = tweakParams.sidewaysFC.asymptoteValue;
				fc.stiffness = tweakParams.sidewaysFC.stiffness;
				w.wc.sidewaysFriction = fc;

				wheels.Add( w );
				return w;
			}
			return null;
		}

		public void AlignWheel( Wheel w, float dt )
		{
			WheelHit hit;
			if ( w.wc.GetGroundHit( out hit ) )
			{
				Vector3 delta = hit.point - w.dummy.position;
				float dn = Vector3.Dot( w.dummy.up, delta );
				w.wt.transform.localPosition = w.dummy.up * ( dn + w.wc.radius );
			}
			else
			{
				Vector3 targetLocalPosition = w.wc.center - Vector3.up * w.wc.suspensionDistance;
				w.wt.transform.localPosition = Vector3.Lerp( w.wt.transform.localPosition, targetLocalPosition, dt * WHEEL_LERP_SPEED );
			}

			float rotSpeed = w.wc.rpm * RPM_2_DEG_PER_SEC;
			float rotDelta = rotSpeed * dt;
			w.rotation += rotDelta;
			w.wt.transform.localRotation = Quaternion.Euler( w.rotation, w.wc.steerAngle, 0f );
		}

		void OnDrawGizmos()
		{
			if ( rigidbody )
			{
				Vector3 world_com = transform.TransformPoint( rigidbody.centerOfMass );
				Gizmos.matrix = Matrix4x4.identity;
				Gizmos.DrawIcon( world_com, "gizmo_com.psd", true );
			}
		}
	}

}