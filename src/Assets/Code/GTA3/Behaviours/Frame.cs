﻿using UnityEngine;
using System.Collections;

namespace GTA3
{
	public class Frame : MonoBehaviour
	{
		public bool drawGizmo = false;
		public float gizmoScale = 0.25f;
		public RwsTexture[] textureNames = new RwsTexture[ 0 ];

		void OnDrawGizmos()
		{
			if ( drawGizmo )
			{
				Gizmos.matrix = transform.localToWorldMatrix * Matrix4x4.Scale( Vector3.one * gizmoScale );

				Gizmos.color = Color.red;
				Gizmos.DrawLine( Vector3.zero, Vector3.right * gizmoScale );

				Gizmos.color = Color.green;
				Gizmos.DrawLine( Vector3.zero, Vector3.up * gizmoScale );

				Gizmos.color = Color.blue;
				Gizmos.DrawLine( Vector3.zero, Vector3.forward * gizmoScale );

				Gizmos.color = Color.white;
				Gizmos.DrawWireCube( Vector3.zero, Vector3.one * gizmoScale );
			}
		}
	}

}