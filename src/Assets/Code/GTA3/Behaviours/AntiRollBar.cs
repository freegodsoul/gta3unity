using UnityEngine;
using System.Collections;

public class AntiRollBar : MonoBehaviour
{
	public WheelCollider wheelL;
	public WheelCollider wheelR;
	public float antiRollCoeff = 0.7f;

	void FixedUpdate()
	{
		bool groundedL;
		bool groundedR;

		float travelL = ComputeTravel( wheelL, out groundedL );
		float travelR = ComputeTravel( wheelR, out groundedR );

		float antiRoll = ( wheelL.suspensionSpring.spring + wheelR.suspensionSpring.spring ) * 0.5f;
		float antiRollForce = ( travelL - travelR ) * antiRoll * antiRollCoeff;

		if ( groundedL )
			rigidbody.AddForceAtPosition( wheelL.transform.up * -antiRollForce, wheelL.transform.position );
		
		if ( groundedR )
			rigidbody.AddForceAtPosition( wheelR.transform.up * antiRollForce, wheelR.transform.position );
	}

	float ComputeTravel( WheelCollider wc, out bool grounded )
	{
		WheelHit hit;
		grounded = wc.GetGroundHit( out hit );
		if ( grounded )
		{
			Vector3 groundLocalPoint = wc.transform.InverseTransformPoint( hit.point ) - wc.center;
			float travel = ( -groundLocalPoint.y - wheelL.radius ) / wheelL.suspensionDistance;
			return travel;
		}
		else
		{
			return 1f;
		}
	}
}