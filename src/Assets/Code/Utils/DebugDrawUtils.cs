using UnityEngine;
using System.Collections;

/// <summary>
/// ������� ��� ��������� Gizmos � ������ ����� � ����� ���������
/// </summary>
public static class DebugDrawUtils
{
	/// <summary>
	/// ������ ������������� �������
	/// </summary>
	/// <param name="rect">����, ������� ���� ����������</param>
	/// <param name="plane">���������, � ������� ���� �����</param>
	public static void DrawWireRect( Rect rect, AxisPlane plane )
	{
		Vector3 v1 = MeshUtils.Plane2World( rect.xMin, rect.yMin, plane );
		Vector3 v2 = MeshUtils.Plane2World( rect.xMax, rect.yMin, plane );
		Vector3 v3 = MeshUtils.Plane2World( rect.xMax, rect.yMax, plane );
		Vector3 v4 = MeshUtils.Plane2World( rect.xMin, rect.yMax, plane );

		Gizmos.DrawLine( v1, v2 );
		Gizmos.DrawLine( v2, v3 );
		Gizmos.DrawLine( v3, v4 );
		Gizmos.DrawLine( v4, v1 );
	}

	public static void DrawSolidPolygon( Vector2[] vertices, AxisPlane plane )
	{
		GL.Begin( GL.TRIANGLES );

		for ( int i = 1; i < vertices.Length - 1; i++ )
		{
			Vector3 v1 = MeshUtils.Plane2World( vertices[ 0 ].x, vertices[ 0 ].y, plane );
			Vector3 v2 = MeshUtils.Plane2World( vertices[ i ].x, vertices[ i ].y, plane );
			Vector3 v3 = MeshUtils.Plane2World( vertices[ i + 1 ].x, vertices[ i + 1 ].y, plane );

			GL.Vertex( v1 );
			GL.Vertex( v2 );
			GL.Vertex( v3 );
		}

		GL.End();
	}

	public static void DrawWirePolygon( Vector2[] vertices, AxisPlane plane, float vertexScale = -1f )
	{
		for ( int i = 0, j = vertices.Length - 1; i < vertices.Length; j = i, i++ )
		{
			Vector3 v1 = MeshUtils.Plane2World( vertices[ i ].x, vertices[ i ].y, plane );
			Vector3 v2 = MeshUtils.Plane2World( vertices[ j ].x, vertices[ j ].y, plane );

			Gizmos.DrawLine( v1, v2 );
		}

		if ( vertexScale > 0f )
		{
			foreach ( Vector2 p in vertices )
			{
				Vector3 v = MeshUtils.Plane2World( p.x, p.y, plane );
				Gizmos.DrawWireCube( v, Vector3.one * vertexScale );
			}
		}
	}

	/// <summary>
	/// ������ ����� ����������
	/// </summary>
	/// <param name="center">����� ����������</param>
	/// <param name="radius">������</param>
	/// <param name="segments">����� ���������, ���������� ����������</param>
	public static void DrawCircleWire( Vector3 center, float radius, int segments )
	{
		float deltaAngle = MathUtils.TWO_PI / (float) segments;

		for ( int i = 0, j = segments - 1; i < segments; j = i, i++ )
		{
			float iAngle = deltaAngle * (float) i;
			float jAngle = deltaAngle * (float) j;

			float ci = Mathf.Cos( iAngle );
			float si = Mathf.Sin( iAngle );

			float cj = Mathf.Cos( jAngle );
			float sj = Mathf.Sin( jAngle );

			Vector3 vi = new Vector3( center.x + ci * radius, center.y + si * radius, center.z );
			Vector3 vj = new Vector3( center.x + cj * radius, center.y + sj * radius, center.z );

			Gizmos.DrawLine( vi, vj );
		}
	}

	/// <summary>
	/// ������ ����
	/// </summary>
	/// <param name="center">����� ��������</param>
	/// <param name="radius">������ ��������</param>
	/// <param name="startAngle">��������� ���� ����</param>
	/// <param name="arcAngle">���� ����</param>
	/// <param name="segments">����� ���������, ��������������� ����</param>
	public static void DrawArc( Vector3 center, float radius, float startAngle, float arcAngle, int segments )
	{
		float deltaAngle = arcAngle / (float) segments;

		for ( int i = 0; i < segments; i++ )
		{
			float iAngle = startAngle + deltaAngle * (float) i;
			float jAngle = startAngle + deltaAngle * (float) ( i + 1 );

			float ci = Mathf.Cos( iAngle );
			float si = Mathf.Sin( iAngle );

			float cj = Mathf.Cos( jAngle );
			float sj = Mathf.Sin( jAngle );

			Vector3 vi = new Vector3( center.x + ci * radius, center.y + si * radius, center.z );
			Vector3 vj = new Vector3( center.x + cj * radius, center.y + sj * radius, center.z );

			Gizmos.DrawLine( vi, vj );
		}
	}
}
