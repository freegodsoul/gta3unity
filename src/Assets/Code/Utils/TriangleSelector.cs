﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TriangleSelector : MonoBehaviour
{
	public int flashSubMesh = 0;
	public int flashTriangleIndex = 0;

	void Update()
	{
		if ( camera )
		{
			RaycastHit hit;
			if ( !Physics.Raycast( camera.ScreenPointToRay( Input.mousePosition ), out hit ) )
				return;

			MeshCollider meshCollider = hit.collider as MeshCollider;
			if ( meshCollider == null || meshCollider.sharedMesh == null )
				return;

			Mesh mesh = meshCollider.sharedMesh;
			DrawTriangle( mesh, hit.collider.transform, hit.triangleIndex );

			if ( Input.GetKeyDown( KeyCode.Mouse0 ) )
				Debug.Log( hit.triangleIndex );
		}

		MeshFilter mf = GetComponent<MeshFilter>();
		if ( mf )
		{
			DrawTriangle( mf.sharedMesh, transform, flashTriangleIndex );
		}
	}

	public void DrawTriangle( Mesh mesh, Transform transform, int index )
	{
		Vector3[] vertices = mesh.vertices;
		int[] triangles = mesh.triangles;

		if ( index < 0 || index >= triangles.Length / 3 )
			return;

		Vector3 p0 = vertices[ triangles[ index * 3 + 0 ] ];
		Vector3 p1 = vertices[ triangles[ index * 3 + 1 ] ];
		Vector3 p2 = vertices[ triangles[ index * 3 + 2 ] ];

		p0 = transform.TransformPoint( p0 );
		p1 = transform.TransformPoint( p1 );
		p2 = transform.TransformPoint( p2 );

		Debug.DrawLine( p0, p1 );
		Debug.DrawLine( p1, p2 );
		Debug.DrawLine( p2, p0 );
	}
}
