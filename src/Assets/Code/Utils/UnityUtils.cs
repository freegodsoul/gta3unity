using UnityEngine;
using System.Collections.Generic;
using System.IO;

public static class UnityUtils
{
	public static Vector3 ScreenPointToWorldXY( Vector3 screenPoint )
	{
		return ScreenPointToWorld( screenPoint, AxisPlane.XY );
	}

	public static Vector3 ScreenPointToWorld( Vector3 screenPoint, AxisPlane axisPlane )
	{
		Ray ray = Camera.main.ScreenPointToRay( screenPoint );
		Plane plane = new Plane( Vector3.forward, 0f );
		switch ( axisPlane )
		{
			case AxisPlane.XY:
				plane = new Plane( Vector3.forward, 0f );
				break;

			case AxisPlane.XZ:
				plane = new Plane( Vector3.up, 0f );
				break;

			case AxisPlane.ZY:
				plane = new Plane( Vector3.right, 0f );
				break;
		}

		float ray_t;

		if ( !Intersection.RayPlaneIntersection( ray, plane, float.PositiveInfinity, out ray_t ) )
			throw new UnityException( "���-�� �� ���!" );

		return ray.GetPoint( ray_t );
	}

	public static Vector2 AlignToOffset( SpriteAlignment align )
	{
		float offsetX = 0f, offsetY = 0f;

		switch ( align )
		{
			case SpriteAlignment.BottomCenter:
				offsetX = 0f;
				offsetY = -1f;
				break;

			case SpriteAlignment.BottomLeft:
				offsetX = -1f;
				offsetY = -1f;
				break;

			case SpriteAlignment.BottomRight:
				offsetX = 1f;
				offsetY = -1f;
				break;

			case SpriteAlignment.LeftCenter:
				offsetX = -1f;
				offsetY = 0f;
				break;

			case SpriteAlignment.RightCenter:
				offsetX = 1f;
				offsetY = 0f;
				break;

			case SpriteAlignment.TopLeft:
				offsetX = -1f;
				offsetY = 1f;
				break;

			case SpriteAlignment.TopCenter:
				offsetX = 0f;
				offsetY = 1f;
				break;

			case SpriteAlignment.TopRight:
				offsetX = 1f;
				offsetY = 1f;
				break;

			case SpriteAlignment.Center:
				offsetX = 0f;
				offsetY = 0f;
				break;
		}

		return new Vector2( offsetX, offsetY );
	}
}