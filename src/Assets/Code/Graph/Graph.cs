﻿using UnityEngine;
using System.Collections.Generic;

public class Graph : MonoBehaviour
{
	public Color color = Color.green;
	public float pointSize = 0.1f;
	public List<Vector2> points = new List<Vector2>();
	private Material mat;

	// Use this for initialization
	void Awake()
	{
		mat = new Material( Shader.Find( "Transparent/SimpleColor" ) );
	}

	// Update is called once per frame
	void OnRenderObject()
	{
		GL.PushMatrix();
		GL.LoadProjectionMatrix( Camera.current.projectionMatrix );
		GL.MultMatrix( Camera.current.worldToCameraMatrix );
		GL.MultMatrix( transform.localToWorldMatrix );

		float sx = 1f  / transform.localScale.x;
		float sy = 1f  / transform.localScale.y;

		// Grid

		mat.color = new Color( 1f, 1f, 1f, 0.5f );
		mat.SetPass( 0 );

		GL.Begin( GL.LINES );
		GL.Vertex3( -10f * sx, 0f, 0f );
		GL.Vertex3( 10f * sx, 0f, 0f );
		GL.Vertex3( 0f, -10f * sy, 0f );
		GL.Vertex3( 0f, 10f * sy, 0f );
		GL.End();


		// Graph

		mat.color = color;
		mat.SetPass( 0 );

		//Gizmos.color = color;
		//GL.Color( color );

		GL.Begin( GL.QUADS );

		float hx = ( pointSize * 0.5f ) *sx;
		float hy = ( pointSize * 0.5f ) *sy;

		for( int i = 0; i < points.Count; i++ )
		{
			Vector2 p0 = points[ i ];

			GL.Vertex3( p0.x - hx, p0.y - hy, 0f );
			GL.Vertex3( p0.x - hx, p0.y + hy, 0f );
			GL.Vertex3( p0.x + hx, p0.y + hy, 0f );
			GL.Vertex3( p0.x + hx, p0.y - hy, 0f );
		}
		GL.End();

		GL.PopMatrix();
	}
}