﻿using UnityEngine;
using System.Collections;

public class CameraViewer : MonoBehaviour
{
	public const float RUN_MULTIPLIER = 4f;
	public const float MAX_TIME_DELTA = 1f / 10f;

	public Transform watchTarget;
	public float watchDistance = 10f;
	public float watchFollowLerpSpeed = 8f;

	public bool useStartRelativePosition = true;

	public float walkSpeed = 25f;
	public float rotationSpeed = 120f;

	public bool useMinLimit = false;
	public float minLimitHeight = 5f;

	// Use this for initialization
	void Start()
	{
		if ( useStartRelativePosition && watchTarget )
		{
			Vector3 D = transform.position - watchTarget.position;
			float watchDistance = D.magnitude;
		}
	}

	// Update is called once per frame
	void Update()
	{
		float dt = Mathf.Min( Time.deltaTime, MAX_TIME_DELTA );

		if ( watchTarget )
		{
			Vector3 targetPosition = watchTarget.position - transform.forward * watchDistance;
			if ( useMinLimit )
				targetPosition.y = Mathf.Max( targetPosition.y, minLimitHeight );

			Vector3 delta = targetPosition - transform.position;
			transform.localPosition += delta * watchFollowLerpSpeed * dt;
		}
		else
		{
			float moveMagnitude = walkSpeed * dt;
			if ( Input.GetKey( KeyCode.LeftShift ) || Input.GetKey( KeyCode.RightShift ) )
				moveMagnitude *= RUN_MULTIPLIER;

			float moveZ = Input.GetAxis( "Vertical" ) * moveMagnitude;
			float moveX = Input.GetAxis( "Horizontal" ) * moveMagnitude;

			transform.Translate( moveX, 0f, moveZ, Space.Self );
		}
		
		if ( Input.GetKey( KeyCode.Mouse1 ) )
		{
			float rotMagnitude = rotationSpeed * dt;
			float rotX = Input.GetAxis( "Mouse Y" ) * rotMagnitude;
			float rotY = Input.GetAxis( "Mouse X" ) * rotMagnitude;

			Vector3 euler = transform.localRotation.eulerAngles;
				
			euler.x -= rotX;
			euler.y += rotY;
			euler.z = 0f;

			if ( watchTarget )
			{
				Vector3 pivotPosition = transform.localPosition + transform.forward * watchDistance;
				transform.localRotation = Quaternion.Euler( euler );
				transform.localPosition = pivotPosition - transform.forward * watchDistance;
			}
			else
			{
				transform.localRotation = Quaternion.Euler( euler );
			}
		}
	}
}